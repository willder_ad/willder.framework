<?php
require_once __DIR__."/bootstrap/config.php";
require_once __DIR__."/bootstrap/helpers.php";
require_once __DIR__."/bootstrap/functions.php";
require_once VENDOR."/autoload.php";

$base = dirname($_SERVER['PHP_SELF']);

if(ltrim($base,'/')){
	$_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($base));
}

$route = new Wolf\Routing\Route();
/*Only for dev*/
$generate_controllers = new Wolf\Generate\GenerateControllers();
require_once APP."/Route.php";

$route->dispatch();