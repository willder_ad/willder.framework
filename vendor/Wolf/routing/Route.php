<?php

namespace Wolf\Routing;

use Klein\Klein as Klein;

class Route extends Klein{
	public function get($route="",$call=null){
		if(is_string($call)){
			$explode = explode("@",$call);
			$controller = "App\\Controllers\\".$explode[0]."Controller";
			$action = $explode[1];
			$this->respond("GET",$route,function($request,$response,$app) use ($controller,$action){
				$controller = new $controller();
				$controller->__loadVars($request,$response,$app);
				return $controller->$action();
			});
		}else{
			$this->respond("GET",$route,$call);
		}
	}
	public function post($route="",$call=null){}
}