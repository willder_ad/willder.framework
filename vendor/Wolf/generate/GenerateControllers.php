<?php

namespace Wolf\Generate;

class GenerateControllers{
	public function __construct(){
		/*
		* Abrindo Diretorios e checando arquivos .phtml
		*/
		$dir = VIEWS;
		$files = scandir($dir);

		/*
		* Separando nomes dos arquivos
		*/
		foreach($files as $file){
			if(!($file == "." || $file == "..")){
				$separator = explode(".", $file);
				$file_name[] = $separator[0];
			}
		}

		for($i = 0;$i < count($file_name);$i++){
			if(!(file_exists(CONTROL."/".$file_name[$i]."Controller.php"))){
				$file = fopen(CONTROL."/{$file_name[$i]}Controller.php","w");
				$controller_context = "<?php\n/*\n*Padrao de Controles devera ser seguido\n*para todos os outros controles.\n*/";
				$controller_context .= "\nnamespace App\Controllers;";
				$controller_context .= "\n";
				$controller_context .= "\nuse Wolf\Http\Controller;";
				$controller_context .= "\n";
				$controller_context .= "\nclass ".ucfirst($file_name[$i])."Controller extends Controller{";
				$controller_context .= "\n	public function {$file_name[$i]}(){";
				$controller_context .= "\n		$"."this->service->render('{$file_name[$i]}.php');";
				$controller_context .= "\n	}";
				$controller_context .= "\n}";

				fwrite($file,$controller_context);
				fclose($file);
			}
		}

	}
}
