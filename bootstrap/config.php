<?php
define("RAIZ", __DIR__."/..");

define("APP",RAIZ."/app");
define("CONTROL",APP."/Controllers");
define("MODELS",APP."/Models");
define("VIEWS",APP."/Views");

define("PUBLIC_HTML",RAIZ."/public_html");

define("VENDOR",RAIZ."/vendor");
define("GENERATE",VENDOR."/Generate");