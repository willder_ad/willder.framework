<?php

function home_url(){
    $protocol = strtolower(preg_replace("/[^a-zA-Z\s]/", "", $_SERVER["SERVER_PROTOCOL"]));
    $name     = $_SERVER["SERVER_NAME"];
    $port 	  = $_SERVER["SERVER_PORT"] == 80 ? "" : ":".$_SERVER["SERVER_PORT"];
    $php_self = str_replace("/init.php","",$_SERVER["PHP_SELF"]);

    return $protocol."://".$name.$port."/frigorifico";
}

function assets_url(){
	$protocol = strtolower(preg_replace("/[^a-zA-Z\s]/", "", $_SERVER["SERVER_PROTOCOL"]));
	$name     = $_SERVER["SERVER_NAME"];
	$port     = $_SERVER["SERVER_PORT"] == 80 ? "" : ":".$_SERVER["SERVER_PORT"];
	$php_self = str_replace("/init.php","",$_SERVER["PHP_SELF"]);

	return home_url()."/public_html";
}

function get_header(){
	require_once PUBLIC_HTML."/includes/header.php";
}

function get_footer(){
	require_once PUBLIC_HTML."/includes/footer.php";
}

function get_templatepart($path,$file){
    require_once PUBLIC_HTML.'/includes/'.$path.'-'.$file.'.php';
}
