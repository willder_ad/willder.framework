<?php

function openConnection(){
    try{
        $connection = mysqli_connect('localhost','root','','frigorifico');
        return $connection;
    }catch(Exception $e){
        throw new Exception('Conexão com o banco de dados falhou: '.$e->getMessage());
    }
}

function checkUser(){
    if(!isset($_SESSION['user'])){
        $_SESSION['danger'] = "Você não está logado para ver esta sessão!";
        header("Location: ".home_url().'/');
        die();
    }
}

function checkUserLogPage(){
    if(isset($_SESSION['user'])){
        echo '<script>window.location.href="'.home_url().'/painel/"</script>';
    }
}

function logIn($email,$senha,$token = false){

    $connection = openConnection();
    $email      = mysqli_real_escape_string($connection,$email);
    $senha      = md5($senha);
    $sql        = "select * from user where email_user = '{$email}' and senha_user = '{$senha}'";
    $res        = false;

    try{
        $result = mysqli_query($connection,$sql);

        if($result->num_rows > 0){
            while($user_array = mysqli_fetch_assoc($result)){
                $_SESSION['user']    = $user_array['email_user'];
                $_SESSION['nick']    = $user_array['user_nickname'];
                $_SESSION['id_user'] = $user_array['id_user'];
                $_SESSION['company'] = $user_array['company'];


                if($token)
                    saveSession();

                App\Model\Config::loadConfig();

                $res = true;
            }
        }else{
            $_SESSION['danger'] = "Login ou senha incorretos!";
            $res = false;
        }
    }catch(Exception $e){
        $res = false;
        throw new Exception('Erro ao logar-se'.$e->getMessage());
    }

    return $res;
}

function saveSession(){
    $connection = openConnection();
    $id_user    = $_SESSION['id_user'];
    $ip_token   = md5(getClientHostIp());
    $email      = $_SESSION['user'];
    $sql        = "insert into ip_log values(null,{$id_user},'{$ip_token}')";

    try{
        mysqli_query($connection,$sql);
    }catch(Exception $e){
        throw new Exception('Error ao salvar sessão: '.$e->getMessage());
    }
}

function getClientHostIp() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function isMobile(){
    $mobile    = false;
    $useragent = $_SERVER['HTTP_USER_AGENT'];

    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
        $mobile = true;

    return $mobile;
}

function checkUserToken(){
    $connection = openConnection();
    $ip_token   = md5(getClientHostIp());
    $sql        = "select email_user,company,user_nickname from ip_log L join user U on (L.id_user = U.id_user) where ip_token = '{$ip_token}'";

    try{
        $result = mysqli_query($connection,$sql);

        if($result->num_rows > 0){
            while($user_array = mysqli_fetch_assoc($result)){
                $_SESSION['user']    = $user_array['email_user'];
                $_SESSION['company'] = $user_array['company'];
                $_SESSION['nick']    = $user_array['user_nickname'];
            }

            header("Location: ".home_url().'/painel/');
        }

    }catch(Exception $e){
        throw new Exception("Error ao redirecionar para sessão: ".$e->getMessage());
    }
}

function logOff(){
    $connection = openConnection();
    $ip_token   = md5(getClientHostIp());
    $sql        = "select * from ip_log where ip_token = '{$ip_token}'";

    try{
        $result = mysqli_query($connection,$sql);

        if($result->num_rows > 0){
            while($user_array = mysqli_fetch_assoc($result)){
                $id_log = $user_array['id_ip_log'];
                $sql     = "delete from ip_log where id_ip_log = {$id_log}";
                mysqli_query($connection,$sql);
            }
        }
    }catch(Exception $e){
        throw new Exception('Erro ao destruir sessão: '.$e->getMessage());
    }
    session_destroy();
    header("Location: ".home_url().'/');
}

function countNotifications(){
    $connection      = openConnection();
    $quantity_notify = 0;
    $company         = $_SESSION['company'];
    $limit_stock     = $_SESSION['limit_stock'];
    $limit_date      = $_SESSION['limit_date'];
    $sql             = "select count(id_product) as quantity from products where (expiration_date < (CURRENT_DATE + {$limit_date}) or stock_quantity_product <= {$limit_stock}) and company = '{$company}'";

    try{
        $result = mysqli_query($connection,$sql);

        while($products_array = mysqli_fetch_assoc($result)){
            $quantity_notify = $products_array['quantity'];
        }
    }catch(Exception $e){
        throw new Exception("Erro ao pegar contagem de notificações: ".$e->getMessage());
    }

    return $quantity_notify;
}

function getProductsCloseToExpirate(){
    $connection = openConnection();
    $products   = array();
    $company    = $_SESSION['company'];
    $limit_date = $_SESSION['limit_date'];
    $sql = "select name_product, if((expiration_date - CURRENT_DATE) < 0, 0, (expiration_date - CURRENT_DATE)) as days_to_expirate from products where expiration_date <= (CURRENT_DATE + {$limit_date}) and company = '{$company}'";

    try{
        $result = mysqli_query($connection,$sql);

        while($products_array = mysqli_fetch_assoc($result)){
            array_push($products,array($products_array['name_product'],$products_array['days_to_expirate']));
        }
    }catch(Exception $e){
        throw new Exception("Error Processing Request".$e->getMessage());
    }

    return $products;
}

function getExpirateProducts(){
    $connection = openConnection();
    $products   = array();
    $company    = $_SESSION['company'];
    $sql        = "select name_product from products where expiration_date < (CURRENT_DATE) and company = '{$company}'";

    try{
        $result = mysqli_query($connection,$sql);

        while($products_array = mysqli_fetch_assoc($result)){
            array_push($products,$products_array['name_product']);
        }
    }catch(Exception $e){
        throw new Exception("Error Processing Request".$e->getMessage());
    }

    return $products;
}

function getProductsCloseToEmpty(){
    $connection  = openConnection();
    $products    = array();
    $company     = $_SESSION['company'];
    $limit_stock = $_SESSION['limit_stock'];
    $sql         = "select name_product, stock_quantity_product from products where stock_quantity_product > 0 and stock_quantity_product <= {$limit_stock} and company = '{$company}'";

    try{
        $result = mysqli_query($connection,$sql);

        while($products_array = mysqli_fetch_assoc($result)){
            array_push($products,array($products_array['name_product'],$products_array['stock_quantity_product']));
        }
    }catch(Exception $e){
        throw new Exception("Error Processing Request".$e->getMessage());
    }

    return $products;
}

function getEmptyProducts(){
    $connection  = openConnection();
    $products    = array();
    $company     = $_SESSION['company'];
    $sql         = "select name_product from products where stock_quantity_product = 0 and company = '{$company}'";

    try{
        $result = mysqli_query($connection,$sql);

        while($products_array = mysqli_fetch_assoc($result)){
            array_push($products,$products_array['name_product']);
        }
    }catch(Exception $e){
        throw new Exception("Error Processing Request".$e->getMessage());
    }

    return $products;
}

function printCupom() {
    // if ($handle = printer_open("EPSON TM-T20 Receipt")) {
    //     printer_set_option($handle, PRINTER_MODE, "RAW");
    //     printer_start_doc($handle);
    //     printer_start_page($handle);
    //     printer_write($handle, $content_body);
    //     printer_end_page($handle);
    //     printer_end_doc($handle);
    //     printer_close($handle);
    // }else {
    //     echo 'Não foi possível abrir a impressora';
    // }
}

function alert($type){
    if(isset($_SESSION[$type])) : ?>
        <p class="alert alert-<?=$type?>" id="alert" style="position: absolute; width:300px;left: 50%;transform: translate(-50%,0);"><?=$_SESSION[$type]?></p>
<?php
    endif;
    unset($_SESSION[$type]);
}
