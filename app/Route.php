<?php
/*
* Abrindo Diretorios e checando arquivos .php
*/
$dir = VIEWS;
$files = scandir($dir);

/*
* Separando nomes dos arquivos
*/
foreach($files as $file){
	if(!($file == "." || $file == "..")){
		$separator = explode(".", $file);
		$file_name[] = $separator[0];
	}
}
/*
* Adicionando rotas
*/
for($i = 0;$i < count($file_name);$i++) {
	if($file_name[$i] == "index")
		$route->get("/","{$file_name[$i]}@".ucfirst($file_name[$i]));
	else
		$route->get("/{$file_name[$i]}/","{$file_name[$i]}@".ucfirst($file_name[$i]));
}