<?php
namespace App\Model;

class ItemSelling{
    /**
     * $id_item id do item
     * @var Int
     */
    private $id_item;
    /**
     * $selling Class Selling
     * @var Selling
     */
    private $selling;
    /**
     * $product Class Produto
     * @var Product
     */
    private $product;
    /**
     * $quantity Quantidade vendida por item
     * @var Int
     */
    private $quantity;

    /**
     * Gets the value of id_item.
     *
     * @return mixed
     */
    public function getIdItem()
    {
        return $this->id_item;
    }

    /**
     * Sets the value of id_item.
     *
     * @param mixed $id_item the id item
     *
     * @return self
     */
    public function _setIdItem($id_item)
    {
        $this->id_item = $id_item;

        return $this;
    }

    /**
     * Gets the value of selling.
     *
     * @return mixed
     */
    public function getSelling()
    {
        return $this->selling;
    }

    /**
     * Sets the value of selling.
     *
     * @param mixed $selling the selling
     *
     * @return self
     */
    public function _setSelling($selling)
    {
        $this->selling = $selling;

        return $this;
    }

    /**
     * Gets the value of produto.
     *
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Sets the value of produto.
     *
     * @param mixed $product the produto
     *
     * @return self
     */
    public function _setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Gets the value of quantity.
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Sets the value of quantity.
     *
     * @param mixed $quantity the quantity
     *
     * @return self
     */
    public function _setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Adiciona itens da venda
     * @param ItemSelling $items Array com valores de item
     */
    public static function addItem(ItemSelling $items)
    {
        $res = false;
        $connection = openConnection();
        $id_selling = mysqli_real_escape_string($connection, $items->getSelling()->getIdSelling());
        $id_product = mysqli_real_escape_string($connection, $items->getProduct()->getIdProduct());
        $quantity   = mysqli_real_escape_string($connection, $items->getQuantity());
        $sql        = "insert into item_selling values(null, {$id_selling}, {$id_product}, {$quantity})";

        try{
            mysqli_query($connection,$sql);

            self::decrementStock($id_product,$quantity);

            $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao guardas items da venda: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Decrementar quantidade atual pela vendida
     * @param  Int $id_product Id do Produto
     * @param  Int $quantity    Quantidade vendida
     * @return void
     */
    public static function decrementStock($id_product, $quantity)
    {
        $connection = openConnection();
        $sql        = "update products set stock_quantity_product = stock_quantity_product - {$quantity} where id_product = {$id_product}";

        try{
            mysqli_query($connection,$sql);
        }catch(Exception $e){
            throw new Exception("Erro ao atualizar estoque: ".$e->getMessage());
        }
    }

    /**
     * Pega todos os itens de uma determinada venda e outros atributos dela
     * @return Array Array com os valores
     */
    public static function getSellingToPrint()
    {
        $connection     = openConnection();
        $id_selling     = Selling::getLastSellingId();
        $sellings_items = array();
        $company        = $_SESSION['company'];
        $sql            = "select prod.bar_code, prod.name_product, item.quantity as selling_quantity, prod.resale_price_product as price, (prod.resale_price_product * item.quantity) as total_item, sell.total_selling as total_venda from sellings sell join item_selling item on (item.id_selling = sell.id_selling) join products prod on (prod.id_product = item.id_product) where prod.company = '{$company}' and sell.id_selling = {$id_selling} order by sell.selling_date";

        try{
            $result = mysqli_query($connection,$sql);

            while($items_array = mysqli_fetch_assoc($result)){
                array_push($sellings_items, array(
                    'bar_code'         => $items_array['bar_code'],
                    'name_product'     => $items_array['name_product'],
                    'selling_quantity' => $items_array['selling_quantity'],
                    'price'            => $items_array['price'],
                    'total_item'       => $items_array['total_item'],
                    'total_venda'      => $items_array['total_venda']
                ));
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer lucro por produto: ".$e->getMessage());
        }

        return $sellings_items;
    }
}
