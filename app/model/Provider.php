<?php

namespace  App\Model;

/**
 * Classe fornecedor onde ficara todo controle de fornecedor
 * @author   willderazevedo434@gmail.com
 * @version 1.0
 */
class Provider {
	/**
	 * $id_provider Id do fornecedor
	 * @var Int
	 */
	private $id_provider;

	/**
	 * $name_provider Nome do fornecedor
	 * @var String
	 */
	private $name_provider;

    /**
     * Gets the $id_provider Id do fornecedor.
     *
     * @return Int
     */
    public function getIdProvider()
    {
        return $this->id_provider;
    }

    /**
     * Sets the $id_provider Id do fornecedor.
     *
     * @param Int $id_provider the id provider
     *
     * @return self
     */
    public function _setIdProvider($id_provider)
    {
        $this->id_provider = $id_provider;

        return $this;
    }

    /**
     * Gets the $name_provider Nome do fornecedor.
     *
     * @return String
     */
    public function getNameProvider()
    {
        return $this->name_provider;
    }

    /**
     * Sets the $name_provider Nome do fornecedor.
     *
     * @param String $name_provider the name provider
     *
     * @return self
     */
    public function _setNameProvider($name_provider)
    {
        $this->name_provider = $name_provider;

        return $this;
    }

    /**
     * Busca todos os fornecedores
     * @return Array Retorna array com os nomes e os ids
     */
    public static function listAllProviders()
    {
    	$providers  = array();
    	$connection = openConnection();
    	$company    = $_SESSION['company'];
    	$sql  		= "select * from provider where company = '{$company}'";

    	try{
    		$result = mysqli_query($connection,$sql);

    		while($providers_array = mysqli_fetch_assoc($result)){
    			$provider = new Provider();
    			$provider->_setIdProvider($providers_array['id_provider']);
    			$provider->_setNameProvider($providers_array['name_provider']);

    			array_push($providers,$provider);
    		}
    	}catch(Exception $e){
    		throw new Exception("Erro ao buscar todos fornecedores: ".$e->getMessage());
    	}

    	return $providers;
    }

    /**
     * Adiciona um novo fornecedor
     * @param Provider $provider Array com atributos do fornecedor
     * @return  Boolean Retorna se deu certo ou não
     */
    public static function addNewProvider(Provider $provider)
    {
    	$res           = false;
    	$connection    = openConnection();
    	$company       = $_SESSION['company'];
    	$name_provider = mysqli_real_escape_string($connection,$provider->getNameProvider());
    	$sql           = "insert into provider values(null,'{$name_provider}','{$company}')";

    	try{
    		mysqli_query($connection,$sql);

    		$res = true;
    	}catch(Exception $e){
    		throw new Exception("Erro ao adicionar fornecedor: ".$e->getMessage());
    	}

    	return $res;
    }

    /**
     * Deleta um fornecedor
     * @param  Int $id_provider Id do fornecedor
     * @return Boolean      Retorna se deu certo ou não
     */
    public static function removeProvider($id_provider)
    {
    	$res         = false;
    	$connection  = openConnection();
    	$id_provider = mysqli_real_escape_string($connection,$id_provider);
    	$sql         = "delete from provider where id_provider = {$id_provider}";

    	try{
    		mysqli_query($connection,$sql);

    		$res = true;
    	}catch(Exception $e){
    		throw new Exception("Erro ao deletar fornecedor: ".$e->getMessage());
    	}

    	return $res;
    }

    /**
     * Edita fornecedor
     * @param  Provider $provider Array com atributos do fornecedor
     * @return Boolean        Retorna se deu certo ou não
     */
    public static function editProvider(Provider $provider)
    {
    	$res           = false;
    	$connection    = openConnection();
    	$id_provider   = mysqli_real_escape_string($connection,$provider->getIdProvider());
    	$name_provider = mysqli_real_escape_string($connection,$provider->getNameProvider());
    	$sql           = "update provider set name_provider = '{$name_provider}' where id_provider = {$id_provider}";

    	try{
    		mysqli_query($connection,$sql);

    		$res = true;
    	}catch(Exception $e){
    		throw new Exception("Erro ao editar fornecedor: ".$e->getMessage());
    	}

    	return $res;
    }
}