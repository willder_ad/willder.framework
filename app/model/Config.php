<?php
namespace App\Model;

class Config{
    /**
     * $id_config Id da configuração
     * @var Int
     */
    private $id_config;
    /**
     * $id_user Id do usuário
     * @var Int
     */
    private $id_user;
    /**
     * $show_notifications Mostra notificações
     * @var Boolean
     */
    private $show_notifications;
    /**
     * $limit_stock Limite pela quantidade no estoque
     * @var Int
     */
    private $limit_stock;
    /**
     * $limit_date Limite pela quantidade de dias restantes
     * @var Int
     */
    private $limit_date;

    /**
     * Gets the $id_config Id da configuração.
     *
     * @return Int
     */
    public function getIdConfig()
    {
        return $this->id_config;
    }

    /**
     * Sets the $id_config Id da configuração.
     *
     * @param Int $id_config the id config
     *
     * @return self
     */
    public function _setIdConfig($id_config)
    {
        $this->id_config = $id_config;

        return $this;
    }

    /**
     * Gets the $id_user Id do usuário.
     *
     * @return Int
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * Sets the $id_user Id do usuário.
     *
     * @param Int $id_user the id user
     *
     * @return self
     */
    public function _setIdUser($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * Gets the $show_notifications Mostra notificações.
     *
     * @return Boolean
     */
    public function getShowNotifications()
    {
        return $this->show_notifications;
    }

    /**
     * Sets the $show_notifications Mostra notificações.
     *
     * @param Boolean $show_notifications the show notifications
     *
     * @return self
     */
    public function _setShowNotifications($show_notifications)
    {
        $this->show_notifications = $show_notifications;

        return $this;
    }

    /**
     * Gets the $limit_stock Limite pela quantidade no estoque.
     *
     * @return Int
     */
    public function getLimitStock()
    {
        return $this->limit_stock;
    }

    /**
     * Sets the $limit_stock Limite pela quantidade no estoque.
     *
     * @param Int $limit_stock the limit stock
     *
     * @return self
     */
    public function _setLimitStock($limit_stock)
    {
        $this->limit_stock = $limit_stock;

        return $this;
    }

    /**
     * Gets the $limit_date Limite pela quantidade de dias restantes.
     *
     * @return Int
     */
    public function getLimitDate()
    {
        return $this->limit_date;
    }

    /**
     * Sets the $limit_date Limite pela quantidade de dias restantes.
     *
     * @param Int $limit_date the limit date
     *
     * @return self
     */
    public function _setLimitDate($limit_date)
    {
        $this->limit_date = $limit_date;

        return $this;
    }

    /**
     * Cadastra ou atualiza as configurações do usuário
     * @param Config $config Array com atributos da classe config
     */
    public function setConfig(Config $config)
    {
        $res                = false;
        $connection         = openConnection();
        $id_user            = $_SESSION['id_user'];
        $show_notifications = mysqli_real_escape_string($connection,$config->getShowNotifications());
        $limit_stock        = mysqli_real_escape_string($connection,$config->getLimitStock());
        $limit_date         = mysqli_real_escape_string($connection,$config->getLimitDate());
        $limit_stock        = ($limit_stock) ? $limit_stock : 0;
        $limit_date         = ($limit_date) ? $limit_date : 0;
        $sql                = "select * from config where id_user = {$id_user}";


        try{
            $result = mysqli_query($connection,$sql);

            if($result->num_rows > 0){
                $sql = "update config set show_notifications = {$show_notifications}, limit_stock = {$limit_stock}, limit_date = {$limit_date} where id_user = {$id_user}";
            }else{
                $sql = "insert into config values(null,{$id_user},{$show_notifications},{$limit_stock},{$limit_date})";
            }

            mysqli_query($connection,$sql);

            $res = true;

            self::loadConfig();
        }catch(Exception $e){
            throw new Exception("Erro ao salvar configurações: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Carrega as configurações do usuário na sessão
     * @return Array
     */
    public static function loadConfig()
    {
        $configs    = array();
        $connection = openConnection();
        $id_user    = $_SESSION['id_user'];
        $sql        = "select * from config where id_user = {$id_user}";

        try{
            $result = mysqli_query($connection,$sql);

            if($result->num_rows > 0){
                while($array_config = mysqli_fetch_assoc($result)){
                    $_SESSION['show_notifications'] = $array_config['show_notifications'];
                    $_SESSION['limit_stock']        = $array_config['limit_stock'];
                    $_SESSION['limit_date']         = $array_config['limit_date'];
                }
            }else{
                $_SESSION['show_notifications'] = false;
                $_SESSION['limit_stock']        = "";
                $_SESSION['limit_date']         = "";
            }
        }catch(Exception $e){
            throw new Exception("Erro ao salvar configurações: ".$e->getMessage());
        }

        return $configs;
    }
}