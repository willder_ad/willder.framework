<?php
namespace App\Model;

/**
 * Classe usuário onde ficara todo controle de usurio exceto login
 * @author   willderazevedo434@gmail.com
 * @version 1.0
 */
class User{
    /**
     * $id_user Id do usuário
     * @var Int
     */
    private $id_user;
    /**
     * $email_user Email de login do usuário
     * @var String
     */
    private $email_user;
    /**
     * $senha_user Senha so usuário
     * @var String
     */
    private $senha_user;
    /**
     * $company Empresa do usuário
     * @var String
     */
    private $company;
    /**
     * $user_nickname Apelido do usuário
     * @var String
     */
    private $user_nickname;

    /**
     * Gets the $id_user Id do usuário.
     *
     * @return Int
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * Sets the $id_user Id do usuário.
     *
     * @param Int $id_user the id user
     *
     * @return self
     */
    public function _setIdUser($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * Gets the $email_user Email de login do usuário.
     *
     * @return String
     */
    public function getEmailUser()
    {
        return $this->email_user;
    }

    /**
     * Sets the $email_user Email de login do usuário.
     *
     * @param String $email_user the email user
     *
     * @return self
     */
    public function _setEmailUser($email_user)
    {
        $this->email_user = $email_user;

        return $this;
    }

    /**
     * Gets the $senha_user Senha so usuário.
     *
     * @return String
     */
    public function getSenhaUser()
    {
        return $this->senha_user;
    }

    /**
     * Sets the $senha_user Senha so usuário.
     *
     * @param String $senha_user the senha user
     *
     * @return self
     */
    public function _setSenhaUser($senha_user)
    {
        $this->senha_user = $senha_user;

        return $this;
    }

    /**
     * Gets the $company Empresa do usuário.
     *
     * @return String
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the $company Empresa do usuário.
     *
     * @param String $company the company
     *
     * @return self
     */
    public function _setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Gets the $user_nickname Apelido do usuário.
     *
     * @return String
     */
    public function getUserNickname()
    {
        return $this->user_nickname;
    }

    /**
     * Sets the $user_nickname Apelido do usuário.
     *
     * @param String $user_nickname the user nickname
     *
     * @return self
     */
    public function _setUserNickname($user_nickname)
    {
        $this->user_nickname = $user_nickname;

        return $this;
    }

    /**
     * Muda as configurações de leitura do usuário Email e Apelido
     * @param  User   $profile Array com atributos do usuário
     * @return Boolean          Retorna se deu certo ou não
     */
    public function changeReadConfig(User $profile)
    {
        $res           = false;
        $connection    = openConnection();
        $id_user       = $_SESSION['id_user'];
        $email_user    = mysqli_real_escape_string($connection,$profile->getEmailUser());
        $user_nickname = mysqli_real_escape_string($connection,$profile->getUserNickname());
        $sql           = "update user set email_user = '{$email_user}', user_nickname = '{$user_nickname}' where id_user = {$id_user}";

        try{
            mysqli_query($connection,$sql);
            $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao salvar perfil: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Muda as configurações de senha do usuário
     * @param  User   $profile Array com atributos do usuário
     * @return Boolean          Retorna se deu certo ou não
     */
    public function changePassConfig(User $profile)
    {
        $res           = false;
        $connection    = openConnection();
        $id_user       = $_SESSION['id_user'];
        $senha_user    = md5($profile->getSenhaUser());
        $sql           = "update user set senha_user = '{$senha_user}' where id_user = {$id_user}";

        try{
            mysqli_query($connection,$sql);
            $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao salvar perfil: ".$e->getMessage());
        }

        return $res;
    }
}
