<?php
namespace App\Model;

/**
 * Classe produto onde ficara todo controle de produto
 * @author   willderazevedo434@gmail.com
 * @version 1.0
 */
class Product{
    /**
     * $id_product Id do produto
     * @var Int
     */
    private $id_product;

    /**
     * Fonecedor
     * @var Provider
     */
    private $provider;

    /**
     * $name_product Nome do produto
     * @var String
     */
    private $name_product;
    /**
     * $purchase_price_product Preço de compra
     * @var Double
     */
    private $purchase_price_product;
    /**
     * $resale_price_product Preço de revenda
     * @var Double
     */
    private $resale_price_product;
    /**
     * $stock_quantity_product Quantidade em estoque
     * @var Double
     */
    private $stock_quantity_product;
    /**
     * $expiration_date Data de expiração do produto
     * @var String
     */
    private $expiration_date;
    /**
     * $type_product Tipo unitario do produto
     * @var String
     */
    private $unit_type_product;
    /**
     * $bar_code Código de barras do produto
     * @var String
     */
    private $bar_code;
    /**
     * $company Empresa que o produto pertence
     * @var String
     */
    private $company;

    /**
     * Gets the value of id_product.
     *
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * Sets the value of id_product.
     *
     * @param mixed $id_product the id product
     *
     * @return self
     */
    public function _setIdProduct($id_product)
    {
        $this->id_product = $id_product;

        return $this;
    }

    /**
     * Gets the Fonecedor.
     *
     * @return Int
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Sets the Fonecedor.
     *
     * @param Provider $provider the provider
     *
     * @return self
     */
    public function _setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Gets the value of name_product.
     *
     * @return mixed
     */
    public function getNameProduct()
    {
        return $this->name_product;
    }

    /**
     * Sets the value of name_product.
     *
     * @param mixed $name_product the name product
     *
     * @return self
     */
    public function _setNameProduct($name_product)
    {
        $this->name_product = $name_product;

        return $this;
    }

    /**
     * Gets the value of unit_price_product.
     *
     * @return mixed
     */
    public function getPurchasePriceProduct()
    {
        return $this->purchase_price_product;
    }

    /**
     * Sets the value of unit_price_product.
     *
     * @param mixed $unit_price_product the unit price product
     *
     * @return self
     */
    public function _setPurchasePriceProduct($purchase_price_product)
    {
        $this->purchase_price_product = $purchase_price_product;

        return $this;
    }

    /**
     * Gets the $resale_price_product Preço de revenda.
     *
     * @return double
     */
    public function getResalePriceProduct()
    {
        return $this->resale_price_product;
    }

    /**
     * Sets the $resale_price_product Preço de revenda.
     *
     * @param double $resale_price_product the resale price product
     *
     * @return self
     */
    public function _setResalePriceProduct($resale_price_product)
    {
        $this->resale_price_product = $resale_price_product;

        return $this;
    }

    /**
     * Gets the value of stock_quantity_product.
     *
     * @return mixed
     */
    public function getStockQuantityProduct()
    {
        return $this->stock_quantity_product;
    }

    /**
     * Sets the value of stock_quantity_product.
     *
     * @param mixed $stock_quantity_product the stock quantity product
     *
     * @return self
     */
    public function _setStockQuantityProduct($stock_quantity_product)
    {
        $this->stock_quantity_product = $stock_quantity_product;

        return $this;
    }

    /**
     * Gets the $type_product Tipo unitario do produto.
     *
     * @return String
     */
    public function getUnitTypeProduct()
    {
        return $this->unit_type_product;
    }

    /**
     * Sets the $type_product Tipo unitario do produto.
     *
     * @param String $unit_type_product the unit type product
     *
     * @return self
     */
    public function _setUnitTypeProduct($unit_type_product)
    {
        $this->unit_type_product = $unit_type_product;

        return $this;
    }

    /**
     * Gets the value of bar_code.
     *
     * @return mixed
     */
    public function getBarCode()
    {
        return $this->bar_code;
    }

    /**
     * Sets the value of bar_code.
     *
     * @param mixed $bar_code the description product
     *
     * @return self
     */
    public function _setBarCode($bar_code)
    {
        $this->bar_code = $bar_code;

        return $this;
    }

    /**
     * Gets the $company Empresa que o produto pertence.
     *
     * @return String
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the $company Empresa que o produto pertence.
     *
     * @param String $company the company
     *
     * @return self
     */
    public function _setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Gets the $expiration_date Data de expiração do produto.
     *
     * @return String
     */
    public function getExpirationDate()
    {
        return $this->expiration_date;
    }

    /**
     * Sets the $expiration_date Data de expiração do produto.
     *
     * @param String $expiration_date the expiration date
     *
     * @return self
     */
    public function _setExpirationDate($expiration_date)
    {
        $this->expiration_date = $expiration_date;

        return $this;
    }

    /**
     * Metodo para inserir novo Produto
     * @param Product Objeto com as propriedades do produto
     * @return boolean Retorna se deu certo ou não
     */
    public function addNewProduct(Product $product)
    {
        $res             = false;
        $connection      = openConnection();
        $name_product    = mysqli_real_escape_string($connection,$product->getNameProduct());
        $purchase_price  = mysqli_real_escape_string($connection,$product->getPurchasePriceProduct());
        $resale_price    = mysqli_real_escape_string($connection,$product->getResalePriceProduct());
        $id_provider     = mysqli_real_escape_string($connection,$product->getProvider()->getIdProvider());
        $stock_quantity  = mysqli_real_escape_string($connection,$product->getStockQuantityProduct());
        $expiration_date = mysqli_real_escape_string($connection,$product->getExpirationDate());
        $unit_type       = mysqli_real_escape_string($connection,$product->getUnitTypeProduct());
        $bar_code        = mysqli_real_escape_string($connection,$product->getBarCode());
        $company         = mysqli_real_escape_string($connection,$product->getCompany());
        $sql             = "insert into products values (0, {$id_provider} ,'{$name_product}', {$purchase_price}, {$resale_price},{$stock_quantity},";

        if($expiration_date){
            $sql .= "'{$expiration_date}',";
        }else{
            $sql .= "NULL,";
        }

        $sql .= "'{$unit_type}', '{$bar_code}', '{$company}')";

        try{
            if(mysqli_query($connection,$sql))
                $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao inserir Produto: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Metodo para editar um Produto
     * @param Product Objeto com as propriedades do produto
     * @return boolean Retorna se deu certo ou não
     */
    public function editProduct(Product $product)
    {
        $res             = false;
        $connection      = openConnection();
        $produto_id      = mysqli_real_escape_string($connection,$product->getIdProduct());
        $name_product    = mysqli_real_escape_string($connection,$product->getNameProduct());
        $purchase_price  = mysqli_real_escape_string($connection,$product->getPurchasePriceProduct());
        $resale_price    = mysqli_real_escape_string($connection,$product->getResalePriceProduct());
        $id_provider     = mysqli_real_escape_string($connection,$product->getProvider()->getIdProvider());
        $stock_quantity  = mysqli_real_escape_string($connection,$product->getStockQuantityProduct());
        $expiration_date = mysqli_real_escape_string($connection,$product->getExpirationDate());
        $unit_type       = mysqli_real_escape_string($connection,$product->getUnitTypeProduct());
        $bar_code        = mysqli_real_escape_string($connection,$product->getBarCode());
        $company         = mysqli_real_escape_string($connection,$product->getCompany());
        $sql             = "update products set id_provider = {$id_provider}, name_product = '{$name_product}', purchase_price_product = {$purchase_price}, resale_price_product = {$resale_price}, stock_quantity_product = {$stock_quantity},";
        if($expiration_date){
            $sql .= "expiration_date = '{$expiration_date}',";
        }else{
            $sql .= "expiration_date = NULL,";
        }

        $sql .= "unit_type_product = '{$unit_type}', bar_code = '{$bar_code}' where id_product = {$produto_id}";

        try{
            if(mysqli_query($connection,$sql))
                $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao editar Produto: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Listar todos os produtos e seus atributos
     * @param  Int Id do produto
     * @return Array Retorna um array de produtos
     */
    public static function listAllProducts($product_id = false)
    {
        $products   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prod.*, prov.* from products prod join provider prov on(prod.id_provider = prov.id_provider) where prod.company = '{$company}'";

        if($product_id){
            $sql .= " and prod.id_product = {$product_id}";
        }

        try{
            $result = mysqli_query($connection,$sql);

            while($products_array = mysqli_fetch_assoc($result)){
                $provider = new Provider();
                $provider->_setIdProvider($products_array['id_provider']);
                $provider->_setNameProvider($products_array['name_provider']);

                $product = new Product();
                $product->_setIdProduct($products_array['id_product']);
                $product->_setProvider($provider);
                $product->_setNameProduct($products_array['name_product']);
                $product->_setPurchasePriceProduct($products_array['purchase_price_product']);
                $product->_setResalePriceProduct($products_array['resale_price_product']);
                $product->_setStockQuantityProduct($products_array['stock_quantity_product']);
                $product->_setExpirationDate($products_array['expiration_date']);
                $product->_setUnitTypeProduct($products_array['unit_type_product']);
                $product->_setBarCode($products_array['bar_code']);

                array_push($products, $product);
            }
        }catch(Exception $e){
            throw new Exception("Erro ao listar produtos: ".$e->getMessage());
        }

        return $products;
    }

    /**
     * Listar todos os produtos e seus atributos
     * @param  Int Id do produto
     * @return Array Retorna um array de produtos
     */
    public static function listAllProductsToSell($bar_code)
    {
        $products   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select * from products where stock_quantity_product > 0 and (expiration_date > (CURRENT_DATE - 1) or expiration_date is null) and company = '{$company}' and bar_code = '{$bar_code}'";

        try{
            $result = mysqli_query($connection,$sql);

            while($products_array = mysqli_fetch_assoc($result)){

                $products = array(
                    "id_product"             => $products_array['id_product'],
                    "name_product"           => $products_array['name_product'],
                    "purchase_price_product" => $products_array['purchase_price_product'],
                    "resale_price_product"   => $products_array['resale_price_product'],
                    "stock_quantity_product" => $products_array['stock_quantity_product'],
                    "expiration_date"        => $products_array['expiration_date'],
                    "unit_type_product"      => $products_array['unit_type_product'],
                    "bar_code"               => $products_array['bar_code'],
                );
            }
        }catch(Exception $e){
            throw new Exception("Erro ao listar produtos: ".$e->getMessage());
        }

        return $products;
    }

    /**
     * Deleta produto selecionado
     * @param  Int $id_product Id do produto selecionado
     * @return Boolean   Retorna se deu certo ou não
     */
    public static function deleteProduct($id_product)
    {
        $connection = openConnection();
        $id_product = mysqli_real_escape_string($connection,$id_product);
        $sql        = "delete from products where id_product = {$id_product}";
        $res        = false;

        try{
            mysqli_query($connection,$sql);
            $res = true;
        }catch(Exception $e){
            throw new Exception('Erro ao excluir produto: '.$e->getMessage());
        }

        return $res;
    }

    /**
     * Função para adicionar a quantidade do produto
     * @param Int $quantity   Quantidade a ser adicionada
     * @param Int $product_id Id do produto a ser atualizado
     * @return  Boolean Retorna se deu certo ou não
     */
    public function addToStock($quantity,$product_id)
    {
        $res        = false;
        $connection = openConnection();
        $quantity   = mysqli_real_escape_string($connection,$quantity);
        $product_id = mysqli_real_escape_string($connection,$product_id);
        $sql        = "update products set stock_quantity_product = stock_quantity_product + {$quantity} where id_product = {$product_id}";

        try{
            mysqli_query($connection,$sql);
            $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao atualizar estoque: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Checa a quantidade de um determinado produto no estoque
     * @param  Int $id_product Id do produto
     * @return Boolean             Retorna se deu certo ou não
     */
    public static function checkProductStock($id_product)
    {
        $res        = false;
        $connection = openConnection();
        $id_product = mysqli_real_escape_string($connection,$id_product);
        $sql        = "select stock_quantity_product from products where id_product = {$id_product}";

        try{
            $result = mysqli_query($connection,$sql);

            while($products_array = mysqli_fetch_assoc($result)){
                if($products_array['stock_quantity_product'] > 0)
                    $res = true;
            }
        }catch(Exception $e){
            throw new Exception("Erro ao consultar estoque: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Checa a data de validade do produto
     * @param  Int $id_product Id do produto
     * @return Boolean             Retorna se deu certo ou não
     */
    public static function checkProductExpiration($id_product){
        $res        = false;
        $connection = openConnection();
        $id_product = mysqli_real_escape_string($connection,$id_product);
        $sql        = "select * from products where expiration_date < (CURRENT_DATE) and id_product = {$id_product}";

        try{
            $result = mysqli_query($connection,$sql);

            if($result->num_rows == 0)
                $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao consultar estoque: ".$e->getMessage());
        }

        return $res;
    }
}
