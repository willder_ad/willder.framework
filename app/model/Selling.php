<?php
namespace App\Model;

/**
 * Classe Venda, onde ficara todo gerenciamente de vendas
 * @author   willderazevedo434@gmail.com
 * @version 1.0
 */
class Selling{
    /**
     * $id_selling Id da venda
     * @var Int
     */
    private $id_selling;
    /**
     * $id_product Id do Produto
     * @var Int
     */
    private $selling_date;
    /**
     * $total_selling Total da venda
     * @var Float
     */
    private $total_selling;

    /**
     * Gets the $id_selling Id da venda.
     *
     * @return Int
     */
    public function getIdSelling()
    {
        return $this->id_selling;
    }

    /**
     * Sets the $id_selling Id da venda.
     *
     * @param Int $id_selling the id selling
     *
     * @return self
     */
    public function _setIdSelling($id_selling)
    {
        $this->id_selling = $id_selling;

        return $this;
    }

    /**
     * Gets the $selling_date Data da venda.
     *
     * @return Date
     */
    public function getSellingDate()
    {
        return $this->selling_date;
    }

    /**
     * Sets the $selling_date Data da venda.
     *
     * @param Date $selling_date the selling date
     *
     * @return self
     */
    public function _setSellingDate($selling_date)
    {
        $this->selling_date = $selling_date;

        return $this;
    }

    /**
     * Gets the $total_selling Total da venda.
     *
     * @return Float
     */
    public function getTotalSelling()
    {
        return $this->total_selling;
    }

    /**
     * Sets the $total_selling Total da venda.
     *
     * @param Float $total_selling the total selling
     *
     * @return self
     */
    public function _setTotalSelling($total_selling)
    {
        $this->total_selling = $total_selling;

        return $this;
    }

    /**
     * Efetuar Venda
     * @param  Selling $selling Array com atributos da venda
     * @return Boolean           Retorna se deu certo ou não
     */
    public function newSelling(Selling $selling)
    {
        $res              = false;
        $connection       = openConnection();
        $selling_date     = mysqli_real_escape_string($connection,$selling->getSellingDate());
        $total_selling    = mysqli_real_escape_string($connection,$selling->getTotalSelling());
        $sql              = "insert into sellings values(null,'{$selling_date}',{$total_selling})";

        try{
            mysqli_query($connection,$sql);

            $res = true;
        }catch(Exception $e){
            throw new Exception("Erro ao efetuar venda: ".$e->getMessage());
        }

        return $res;
    }

    /**
     * Listar todos as vendas e seus atributos
     * @return Array Retorna um array de vendas
     */
    public static function listAllSellings()
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prod.name_product as product, sum(item.quantity) quantity, sell.selling_date date from sellings sell join item_selling item on (item.id_selling = sell.id_selling) join products prod on (item.id_product = prod.id_product) where prod.company = '{$company}' group by prod.name_product";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings, array($sellings_array['product'],$sellings_array['quantity'],$sellings_array['date']));
            }
        }catch(Exception $e){
            throw new Exception("Erro ao listar produtos: ".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Pega total de tudo media de lucro, total dos lucros, total bruto da venda
     * @param  String $data_begin Data de inicio
     * @param  String $data_end   Data final
     * @return Array             Retorna os valores em um array
     */
    public static function getTotalOfAll($data_begin, $data_end)
    {
        $sellings = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $data_begin = mysqli_real_escape_string($connection,$data_begin);
        $data_end   = mysqli_real_escape_string($connection,$data_end);
        $sql        = "select avg((((item.quantity * prod.resale_price_product) - (item.quantity * prod.purchase_price_product)) * 100) / prod.purchase_price_product) as media_lucro, sum((item.quantity * prod.resale_price_product) - (item.quantity * prod.purchase_price_product)) as total_lucro, sum(sell.total_selling) as total_bruto from products prod join item_selling item on (item.id_product = prod.id_product) join sellings sell on (sell.id_selling = item.id_selling) where prod.company = '{$company}' and sell.selling_date BETWEEN '{$data_begin}' and '{$data_end}'";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,$sellings_array['media_lucro'],$sellings_array['total_lucro'],$sellings_array['total_bruto']);
            }
        }catch(Exception $e){
            throw new Exception("Erro ao buscar vendas por período: ".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Pega todas as vendas de um determinado período
     * @param  String $data_begin Data de inicio
     * @param  String $data_end   Data final
     * @return Array             Retorna os valores em uma rray
     */
    public static function getVendasPerPeriod($data_begin, $data_end)
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $data_begin = mysqli_real_escape_string($connection,$data_begin);
        $data_end   = mysqli_real_escape_string($connection,$data_end);
        $sql = "select prod.name_product as name,prod.purchase_price_product preco_compra ,prod.resale_price_product preco_revenda, item.quantity as quantity, sell.selling_date as date, ((item.quantity * prod.resale_price_product) - (item.quantity * prod.purchase_price_product)) as lucro_real, (prod.resale_price_product * item.quantity) as total from products prod join item_selling item on (item.id_product = prod.id_product) join sellings sell on (sell.id_selling = item.id_selling) where prod.company = '{$company}' and sell.selling_date BETWEEN '{$data_begin}' and '{$data_end}'";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,array($sellings_array['name'],$sellings_array['preco_compra'],$sellings_array['preco_revenda'],$sellings_array['quantity'],$sellings_array['date'],$sellings_array['lucro_real'],$sellings_array['total']));
            }
        }catch(Exception $e){
            throw new Exception("Erro ao buscar vendas por período: ".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Retorna o produto mais vendido e seus atributos
     * @return Array Array com atributos do produto mais vendido
     */
    public static function getProductMoreSelled()
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prod.name_product as name,sum(item.quantity) as sell_quantity, prod.stock_quantity_product as stock, sum(sell.total_selling) as total from sellings sell join item_selling item on (item.id_selling = sell.id_selling) join products prod on (item.id_product = prod.id_product) where prod.company = '{$company}' group by prod.name_product order by sell_quantity desc limit 1";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,
                    $sellings_array['name'],
                    number_format($sellings_array['sell_quantity'],2),
                    number_format($sellings_array['stock'],2),
                    number_format($sellings_array['total'],2));
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer produto mais vendido: ".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Busca o ultimo id da venda cadastrado
     * @return Int Retorna o id
     */
    public static function getLastSellingId()
    {
        $connection = openConnection();
        $id_selling = false;
        $sql        = "select max(id_selling) last_id from sellings";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                $id_selling = $sellings_array['last_id'];
            }
        }catch(Exception $e){
            throw new Exception("Erro ao buscar id: ".$e->getMessage());
        }

        return $id_selling;
    }

    /**
     * Retorna o produtos mais vendido e seus atributos
     * @return Array Array com atributos do produto mais vendido
     */
    public static function getProductsMoreSelled()
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prod.name_product as name,sum(item.quantity) as sell_quantity from sellings sell join item_selling item on (item.id_selling = sell.id_selling) join products prod on (item.id_product = prod.id_product) where prod.company = '{$company}' group by prod.name_product order by sell_quantity desc limit 6";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,array(
                        'product' => $sellings_array['name'],
                        'quantity' => number_format($sellings_array['sell_quantity'],2),
                    )
                );
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer produtos mais vendidos:".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Retorna o produtos mais vendido e seus atributos
     * @return Array Array com atributos do produto mais vendido
     */
    public static function getBestProviders()
    {
        $providers  = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prov.name_provider as name, avg(prod.purchase_price_product) as purchase from products prod join provider prov on (prov.id_provider = prod.id_provider) where prod.company = '{$company}' group by prov.name_provider order by name asc limit 6";

        try{
            $result = mysqli_query($connection,$sql);

            while($providers_array = mysqli_fetch_assoc($result)){
                array_push($providers,array(
                        'provider' => $providers_array['name'],
                        'purchase' => number_format($providers_array['purchase'],2),
                    )
                );
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer fornecedores em conta: ".$e->getMessage());
        }

        return $providers;
    }

    /**
     * Pega todos os as vendas do dia e sua quantidade
     * @return Array Array com os valores da venda
     */
    public static function getSellingPerDay()
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select selling_date date,count(sell.id_selling) as quantity from sellings sell join item_selling item on (item.id_selling = sell.id_selling) join products prod on (prod.id_product = item.id_product) where prod.company = '{$company}' group by 1 order by sell.selling_date asc";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,array(
                        'day'      => date('d/m/Y',strtotime($sellings_array['date'])),
                        'quantity' => $sellings_array['quantity']
                    )
                );
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer as vendas por dia: ".$e->getMessage());
        }

        return $sellings;
    }

    /**
     * Pega o lucro por de cada produto
     * @return Array Retorna o array com os valores do nome e do lucro
     */
    public static function getLucroPerProduct()
    {
        $sellings   = array();
        $connection = openConnection();
        $company    = $_SESSION['company'];
        $sql        = "select prod.name_product name, avg((((item.quantity * prod.resale_price_product) - (item.quantity * prod.purchase_price_product)) * 100) / prod.purchase_price_product) as media_lucro from products prod join item_selling item on (item.id_product = prod.id_product) join sellings sell on (item.id_selling = sell.id_selling) where prod.company = '{$company}' group by 1 limit 6";

        try{
            $result = mysqli_query($connection,$sql);

            while($sellings_array = mysqli_fetch_assoc($result)){
                array_push($sellings,array(
                        'label'=> $sellings_array['name'],
                        'data' => number_format($sellings_array['media_lucro'],2)
                    )
                );
            }
        }catch(Exception $e){
            throw new Exception("Erro ao trazer lucro por produto: ".$e->getMessage());
        }

        return $sellings;
    }
}