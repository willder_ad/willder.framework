<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class RelatorioController extends Controller{
	public function relatorio(){
		$this->service->render('relatorio.php');
	}
}