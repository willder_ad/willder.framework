<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class PerfilController extends Controller{
	public function perfil(){
		$this->service->render('perfil.php');
	}
}