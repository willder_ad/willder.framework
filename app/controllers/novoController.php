<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class NovoController extends Controller{
	public function novo(){
		$this->service->render('novo.php');
	}
}