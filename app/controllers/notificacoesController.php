<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class NotificacoesController extends Controller{
	public function notificacoes(){
		$this->service->render('notificacoes.php');
	}
}