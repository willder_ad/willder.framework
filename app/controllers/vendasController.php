<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class VendasController extends Controller{
	public function vendas(){
		$this->service->render('vendas.php');
	}
}