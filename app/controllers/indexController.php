<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class IndexController extends Controller{
	public function index(){
		$this->service->render("index.php");
	}
}