<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class EstoqueController extends Controller{
	public function estoque(){
		$this->service->render('estoque.php');
	}
}