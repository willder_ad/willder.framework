<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class PainelController extends Controller{
	public function painel(){
		$this->service->render('painel.php');
	}
}