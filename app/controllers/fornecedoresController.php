<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class FornecedoresController extends Controller{
	public function fornecedores(){
		$this->service->render('fornecedores.php');
	}
}