<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class CaixaController extends Controller{
	public function caixa(){
		$this->service->render('caixa.php');
	}
}