<?php
/*
*Padrao de Controles devera ser seguido
*para todos os outros controles.
*/
namespace App\Controllers;

use Wolf\Http\Controller;

class EditarController extends Controller{
	public function editar(){
		$this->service->render('editar.php');
	}
}