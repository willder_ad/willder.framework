<?php get_header(); ?>
<div class="page-wrapper">
	<div class="container-fluid">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-plus-circle"></i>
                    Novo <small>produto</small>
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
	        <div class="panel-heading">
	            <h3 class="panel-title"><i class="fa fa-cube fa-fw"></i> Produto</h3>
	        </div>
	        <div class="panel-body">
		        <div class="row">
		        	<div class="col-lg-12">
		        		<form action="<?=assets_url()?>/scripts/add-product.php" method="post" role="form">
		        			<div class="form-group">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <label for="nome-produto">
                                            Nome do produto
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="nome-produto" class="form-control" placeholder="Ex: Doce de Leite" required>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="tipo-unitario-produto">
                                            Tipo
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select name="tipo-unitario-produto" class="form-control">
                                            <option value="kg">Kilograma</option>
                                            <option value="unit">Unidade</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="preco-unitario">
                                            Preço Compra (unidade/kg)
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="number" name="preco-compra" class="form-control" placeholder="Ex: 20" min="0.1" step="any" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="preco-unitario">
                                            Preço Revenda (unidade/kg)
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="number" name="preco-revenda" class="form-control" placeholder="Ex: 20" min="0.1" step="any" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label for="fornecedor">
                                            Fornecedor
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select name="fornecedor" class="form-control" required>
                                            <option value="">Selecione</option>
                                            <?php
                                            $fornecedores = App\Model\Provider::listAllProviders();

                                            if(!empty($fornecedores)):
                                                foreach($fornecedores as $fornecedor):
                                            ?>
                                                <option value="<?=$fornecedor->getIdProvider()?>">
                                                    <?=$fornecedor->getNameProvider()?>
                                                </option>
                                            <?php endforeach; else: ?>
                                                <option value="">Nenhum fornecedor cadastrado</option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="quantidade_estoque">
                                            Quantidade no Estoque
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="number" name="quantidade-estoque" class="form-control" placeholder="Ex: 2, 0.5, 10" min="0.1" step="any" required>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="data-validade">
                                            Data de válidade
                                        </label>
                                        <input type="date" name="data-validade" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="codigo-barra">
                                    Código de barras do Produto
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="number" step="1" min="1" name="codigo-barra" class="form-control" required>
                            </div>

		        			<div class="form-group">
								<div>
									<span class="text-danger">*</span> Campo obrigátorio
								</div>
                                <input type="submit" class="btn btn-success" value="Cadastrar">
		                    </div>
		        		</form>
		        	</div>
		        </div>
		    </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>