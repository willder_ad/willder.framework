<?php get_header(); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-file-text"></i>
                    Relatório
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-file-text fa-fw"></i>
                    Relatório de margem de lucro por intervalo de datas
                </h3>
            </div>
            <div class="panel-body">
                <form action="<?=assets_url()?>/scripts/pdf-generator.php" method="post" role="form">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>
                                    Data de inicio
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="date" class="form-control date_begin" name="data-inicio" required>
                            </div>
                            <div class="col-lg-6">
                                <label>
                                    Data Final
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="date" class="form-control date_end" name="data-final" data-toggle="popover" data-placement="top" data-content="Data final deve ser maior que a inicial" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <span class="text-danger">*</span>
                                Campo obrigatório
                            </div>
                            <div class="pull-right">
                                <div class="col-lg-6">
                                    <input type="submit" class="btn btn-primary" value="Gerar">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>