<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Controle de Estoque</title>
    <link href="<?=assets_url()?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/sb-admin.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/morris.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <?php session_start(); checkUserToken(); checkUserLogPage(); ?>
</head>
<body>
    <div class="container">
      <div class="row">
        <div class="center-block text-center">
          <?php alert('danger'); ?>
        </div>
      </div>
      <form class="form-signin" action="<?=assets_url()?>/scripts/login.php" method="POST">
        <h2 class="form-signin-heading">Login</h2>
        <label for="inputEmail" class="sr-only">Login</label>
        <input type="text" id="inputEmail" name="email" class="form-control" placeholder="Usuário" required autofocus>
        <label for="inputPassword" class="sr-only">Senha</label>
        <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Senha" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="lembrar">Lembrar de mim
            </label>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
      </form>
    </div>
    <script src="<?=assets_url()?>/js/jquery.js"></script>
    <script type="text/javascript">
        setTimeout(function(){jQuery("#alert").fadeOut()},2000);
    </script>
</body>
</html>