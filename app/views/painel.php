<?php get_header(); ?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-dashboard"></i>
                    Painel <small>inicial</small>
                </h1>
            </div>
        </div>
        <div class="row">
            <?php get_templatepart('widgets/widget','produto-vendido'); ?>

            <?php get_templatepart('widgets/widget','vendas-no-mes'); ?>

            <?php get_templatepart('widgets/widget','produtos-vendidos'); ?>

            <?php get_templatepart('widgets/widget','best-providers'); ?>

            <?php get_templatepart('widgets/widget','produto-lucro'); ?>
        </div>
    </div>
</div>
<script src="<?=assets_url()?>/js/raphael.min.js"></script>
<script src="<?=assets_url()?>/js/morris.min.js"></script>
<script src="<?=assets_url()?>/js/morris-data.js"></script>
<script src="<?=assets_url()?>/js/jquery.flot.js"></script>
<script src="<?=assets_url()?>/js/jquery.flot.pie.js"></script>
<script src="<?=assets_url()?>/js/jquery.flot.resize.js"></script>
<script src="<?=assets_url()?>/js/jquery.flot.tooltip.min.js"></script>
<?php get_footer(); ?>