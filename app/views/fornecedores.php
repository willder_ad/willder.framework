<?php get_header(); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-users"></i>
                    Todos <small>Fornecedores</small>
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelSearch">
                        <i class="fa fa-search fa-fw"></i>
                        Filtrar por pesquisa
                    </a>
                </h3>
            </div>
            <div id="collapsePainelSearch" class="panel-collapse collapse">
                <div class="panel-body">
                    <label>Nome do fornecedor</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="search form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-users fa-fw"></i>
                    Novo Fornecedor
                </h3>
            </div>
            <div class="panel-body">
                <form action="<?=assets_url()?>/scripts/add-provider.php" role="form" method="post">
                    <div class="row">
                        <div class="col-lg-11">
                            <div class="form-group">
                                <label>
                                    Nome do fornecedor
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="name-provider" required>
                            </div>
                        </div>
                        <div class="col-lg-1" style="margin-top: 25px;">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary form-control" title="Adicionar">
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h3 class="panel-title" style="margin-top: 10px !important;">
                            <i class="fa fa-cubes fa-fw"></i> Produtos
                        </h3>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="pull-right">
                            <select id="filter" class="form-control">
                                <option value="">Filtrar</option>
                                <option value="asc_name@1">Nome (A-Z)</option>
                                <option value="desc_name@1">Nome (Z-A)</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table-stock">
                                <thead class="text-center" style="background-color: #333; color: #fff;">
                                    <tr>
                                        <td>Nome do Fornecedor</td>
                                        <td colspan="2">Ações</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $fornecedores = App\Model\Provider::listAllProviders();
                                    $count        = 1;

                                    if(!empty($fornecedores)):
                                        foreach($fornecedores as $fornecedor):
                                    ?>
                                        <tr>
                                            <td><?=$fornecedor->getNameProvider()?></td>
                                            
                                            <td width="10%" class="text-center">
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-<?=$count?>">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                            </td>

                                            <div class="modal fade" id="modal-<?=$count?>" tabindex="-1" role="dialog" aria-labelledby="title">
                                                <div class="modal-dialog modal-sm" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h4 class="modal-title" id="title">
                                                                Editar Fornecedor
                                                            </h4>
                                                        </div>
                                                        <form action="<?=assets_url()?>/scripts/edit-provider.php" method="post" role="form">
                                                            <div class="modal-body">
                                                                <label for="quantidade">
                                                                    Nome Fornecedor
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <input type="text" name="name-provider" class="form-control" placeholder="Digite aqui" value="<?=$fornecedor->getNameProvider()?>" required>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <input type="text" value="<?=$fornecedor->getIdProvider()?>" class="hidden" name="id-provider">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
                                                                    Cancelar
                                                                </button>
                                                                <input type="submit" class="btn btn-primary" title="Adicionar ao estoque" value="Editar">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <td width="10%" class="text-center">
                                                <form action="<?=assets_url()?>/scripts/remove-provider.php" role="form" method="post" onsubmit="return choice();">
                                                    <input type="hidden" name="id-provider" value="<?=$fornecedor->getIdProvider()?>">
                                                    <button type="submit" class="btn btn-danger" title="Remover">
                                                        <i class="fa fa-remove"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php $count++; endforeach; else: ?>
                                        <tr>
                                            <td colspan="2">
                                                Nenhum fornecedor cadastrado
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table = $('.panel-body #table-stock tbody');
    var rows = table.find('tr');
</script>
<script type="text/javascript" src="<?=assets_url()?>/js/filter.js"></script>
<script type="text/javascript" src="<?=assets_url()?>/js/estoque-filtro.js"></script>
<?php get_footer(); ?>