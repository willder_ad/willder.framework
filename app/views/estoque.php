<?php get_header(); ?>
<div class="page-wrapper">
	<div class="container-fluid">
		<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-cubes"></i>
                    Todos <small>produtos</small>
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelSearch">
                        <i class="fa fa-search fa-fw"></i>
                        Filtrar por pesquisa
                    </a>
                </h3>
            </div>
            <div id="collapsePainelSearch" class="panel-collapse collapse">
                <div class="panel-body">
                	<label>Nome do produto</label>
                    <div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-search"></i>
						</span>
						<input type="text" class="search form-control">
					</div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
	        <div class="panel-heading">
	        	<div class="row">
	        		<div class="col-xs-6 col-sm-6 col-md-6">
	            		<h3 class="panel-title" style="margin-top: 10px !important;">
	            			<i class="fa fa-cubes fa-fw"></i> Produtos
	            		</h3>
	        		</div>
	        		<div class="col-xs-6 col-sm-6 col-md-6">
			            <div class="pull-right">
							<select id="filter" class="form-control">
								<option value="">Filtrar</option>
								<option value="asc_name@1">Nome (A-Z)</option>
								<option value="desc_name@1">Nome (Z-A)</option>
								<option value="asc_name@2">Fornecedor (A-Z)</option>
								<option value="desc_name@2">Fornecedor (Z-A)</option>
								<option value="desc_price@5">Maior Preço (Revenda)</option>
								<option value="asc_price@5">Menor Preço (Revenda)</option>
								<option value="desc_price@4">Maior Preço (Compra)</option>
								<option value="asc_price@4">Menor Preço (Compra)</option>
								<option value="desc_quantity@3">Maior Quantidade</option>
								<option value="asc_quantity@3">Menor Quantidade</option>
								<option value="date@6">Data de validade</option>
							</select>
			            </div>
	        		</div>
	        	</div>
	        </div>
	        <div class="panel-body">
	        	<div class="row">
	        		<div class="col-lg-12">
		        		<div class="table-responsive">
							<table class="table table-striped table-bordered" id="table-stock">
								<thead class="text-center" style="background-color: #333; color: #fff;">
									<tr>
										<td>Nome do produto</td>
										<td>Fornecedor</td>
										<td>Quantidade (Unidade/Kg)</td>
										<td>Preço R$ (compra)</td>
										<td>Preço R$ (revenda)</td>
										<td>Validade</td>
										<td colspan="3">Ações</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$produtos = App\Model\Product::listAllProducts();
									$count    = 1;

									if(!empty($produtos)):
										foreach($produtos as $produto):
											$data_validade = $produto->getExpirationDate();
									?>
											<tr>
												<td><?=$produto->getNameProduct()?></td>
												<td><?=$produto->getProvider()->getNameProvider()?></td>

												<?php if($produto->getUnitTypeProduct() == "kg"): ?>
													<td class="text-center">
														<?=number_format($produto->getStockQuantityProduct(), 2, ',', '')?>
													</td>
												<?php else: ?>
													<td class="text-center">
														<?=$produto->getStockQuantityProduct()?>
													</td>
												<?php endif; ?>
												<td class="text-center">
													<?=$produto->getPurchasePriceProduct()?>
												</td>
												<td class="text-center">
													<?=$produto->getResalePriceProduct()?>
												</td>
												<td>
													<?=($data_validade)?date('d/m/Y',strtotime($data_validade)):''?>
												</td>

												<td width="10%" class="text-center">
													<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-<?=$count?>" title="Adicioar ao estoque">
														<i class="fa fa-plus-circle"></i>
													</button>
												</td>

												<div class="modal fade" id="modal-<?=$count?>" tabindex="-1" role="dialog" aria-labelledby="title">
													<div class="modal-dialog modal-sm" role="document">
														<div class="modal-content">
															<div class="modal-header">
														        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
														        	<span aria-hidden="true">&times;</span>
														        </button>
														        <h4 class="modal-title" id="title">
														        	Adicionar ao Estoque
														        </h4>
													        </div>
									            			<form action="<?=assets_url()?>/scripts/add-stock.php" method="post" role="form">
														        <div class="modal-body">
																	<label for="quantidade">
												                        Quantidade
												                        <span class="text-danger">*</span>
												                    </label>
												                    <input type="number" name="quantidade" class="form-control" min="0.01" step="any" placeholder="Ex: 1, 0.5, 3" required>
														        </div>
														        <div class="modal-footer">
														        	<input type="text" value="<?=$produto->getIdProduct()?>" class="hidden" name="produto-id">
														        	<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
														        		Cancelar
															        </button>
										                    		<input type="submit" class="btn btn-primary" title="Adicionar ao estoque" value="Adicionar">
														        </div>
														    </form>
														</div>
													</div>
												</div>

												<td width="10%" class="text-center">
													<form action="<?=assets_url()?>/scripts/edit-redirect.php" method="post" role="form">
														<input type="text" value="<?=$produto->getIdProduct()?>" class="hidden" name="produto-id">
														<button type="submit" class="btn btn-warning" title="Editar">
															<i class="fa fa-pencil"></i>
														</button>
													</form>
												</td>

												<td width="10%" class="text-center">
													<form action="<?=assets_url()?>/scripts/remove-product.php" method="post" role="form" onsubmit="return choice();">
														<input type="text" value="<?=$produto->getIdProduct()?>" class="hidden" name="produto-id">
														<button type="submit" class="btn btn-danger" title="Remover">
															<i class="fa fa-close"></i>
														</button>
													</form>
												</td>
											</tr>
									<?php $count++; endforeach; else: ?>
										<tr>
											<td colspan="7">Nenhum produto em estoque</td>
										</tr>
									<?php endif; ?>
								</tbody>
							</table>
		        		</div>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	var table = $('.panel-body #table-stock tbody');
    var rows = table.find('tr');
</script>
<script type="text/javascript" src="<?=assets_url()?>/js/filter.js"></script>
<script type="text/javascript" src="<?=assets_url()?>/js/estoque-filtro.js"></script>
<?php get_footer(); ?>