<?php get_header(); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-gear"></i>
                    Perfil
                </h1>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelOne">
                        <i class="fa fa-user fa-fw"></i>
                        Configurações de exibição
                    </a>
                </h3>
            </div>
            <div id="collapsePainelOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <form action="<?=assets_url()?>/scripts/edit-profile.php" method="post" role="form">
                        <input type="hidden" name="type" value="1">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="user-email">Meu email de login</label>
                                    <input type="text" name="user-email" placeholder="Email de login" class="form-control" value="<?=$_SESSION['user']?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nickname">Meu apelido</label>
                                    <input type="text" name="user-nickname" placeholder="Um nome que gostaria de usar" class="form-control" value="<?=$_SESSION['nick']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-primary" value="Salvar Perfil">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelTwo">
                        <i class="fa fa-asterisk fa-fw"></i>
                        Configuração de Senha
                    </a>
                </h3>
            </div>
            <div id="collapsePainelTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <form action="<?=assets_url()?>/scripts/edit-profile.php" method="post" role="form">
                        <input type="hidden" name="type" value="2">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="new-pass">Nova Senha</label>
                                    <input type="password" name="new-pass" class="form-control new_pass" placeholder="Nova Senha" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="confirm-new-pass">Confirmar nova Senha</label>
                                    <input type="password" name="confirm-new-pass" class="form-control confirm_new_pass" placeholder="Confirmar nova Senha" data-toggle="popover" data-placement="top" data-content="Senha diferente da digitada." required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-primary" value="Salvar Perfil">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelThree">
                        <i class="fa fa-bell fa-fw"></i>
                        Configuração de Notificações
                    </a>
                </h3>
            </div>
            <div id="collapsePainelThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <form action="<?=assets_url()?>/scripts/edit-profile.php" method="post" role="form">
                        <input type="hidden" name="type" value="3">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="show-notification">Mostrar notificações</label>
                                    <div class="radio-group">
                                        <input type="radio" value="true" name="show-notification" <?=($_SESSION['show_notifications'])?'checked':''?> required>&nbsp;Sim<br>
                                        <input type="radio" value="false" name="show-notification" <?=!($_SESSION['show_notifications'])?'checked':''?> required>&nbsp;Não
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="quantity-limit">Faltando N itens no estoque</label>
                                    <input type="number" step="1" min="1" name="quantity-limit" class="form-control quantity_limit" placeholder="Ex: 1, 2" value="<?=$_SESSION['limit_stock']?>" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="date-limit">Faltando N dias para fim de válidade</label>
                                    <input type="number" step="1" min="1" name="date-limit" class="form-control date_limit" placeholder="Ex: 1, 2" value="<?=$_SESSION['limit_date']?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="pull-right">
                                <div class="col-lg-12">
                                    <input type="submit" class="btn btn-primary" value="Salvar Perfil">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>