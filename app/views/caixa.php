<?php
if(isMobile())
    header("Location: ".home_url());

get_header();
?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-money"></i>
                    Caixa
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-cube fa-fw"></i> Produto
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <input type="hidden" value="" class="id-product">
                    <input type="hidden" value="" class="product-price">
                    <div class="col-lg-3">
                        <label for="codigo-barras">Código de barras</label>
                        <input type="number" step="1" min="1" name="codigo-barras" class="bar-code form-control" data-toggle="bar-code" data-placement="top" data-content="">
                    </div>
                    <div class="col-lg-5">
                        <label for="nome-produto">Nome do produto</label>
                        <input type="text" name="nome-produto" class="name-product form-control" readonly>
                    </div>
                    <div class="col-lg-2">
                        <label for="quantidade">Estoque</label>
                        <input type="number" step="any" min="0.1" class="stock-quantity form-control" readonly>
                    </div>
                    <div class="col-lg-2">
                        <label for="quantidade">Quantidade</label>
                        <input type="number" step="any" min="0.1" value="1" class="sell-quantity form-control" data-toggle="popover" data-placement="top" data-content="Quantidade inválida!">
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-shopping-cart fa-fw"></i> Carrinho de compras
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table-cart">
                                <thead class="text-center" style="background-color: #333; color: #fff;">
                                    <tr>
                                        <td>Nome do produto</td>
                                        <td>Quantidade</td>
                                        <td>Preço R$</td>
                                        <td>
                                            <i class="fa fa-shopping-cart"></i>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="default">
                                        <td colspan="4">Nenhum item no carrinho</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="pull-right ">
                            Total da venda: R$
                            <span class="total">
                                0,00
                            </span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="button" class="btn btn-primary pull-right end-selling" value="Finalizar venda" data-toggle="end-sell" data-placement="top" data-content="Não foi possível finalizar a venda, nenhum item no carrinho">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var assets_url = "<?=assets_url()?>";
</script>
<script type="text/javascript" src="<?=assets_url()?>/js/ajax.js"></script>
<script type="text/javascript" src="<?=assets_url()?>/js/sellings-functions.js"></script>
<?php get_footer(); ?>