<?php get_header(); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-bell"></i>
                    Todas <small>Notificações</small>
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelSearch">
                        <i class="fa fa-search fa-fw"></i>
                        Filtrar por pesquisa
                    </a>
                </h3>
            </div>
            <div id="collapsePainelSearch" class="panel-collapse collapse">
                <div class="panel-body">
                    <label>Nome do produto</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="search form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h3 class="panel-title" style="margin-top: 10px !important;">
                            <i class="fa fa-bell fa-fw"></i> Notificações
                        </h3>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="pull-right">
                            <select id="filter" class="form-control">
                                <option value="">Filtrar</option>
                                <option value="asc_name@1">Nome (A-Z)</option>
                                <option value="desc_name@1">Nome (Z-A)</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table-stock">
                                <thead class="text-center" style="background-color: #333; color: #fff;">
                                    <tr>
                                        <td>Nome do produto</td>
                                        <td width="20%">Status</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $produtos_para_vencer = getProductsCloseToExpirate();

                                    if(!empty($produtos_para_vencer)):
                                        foreach($produtos_para_vencer as $produto):
                                    ?>
                                            <tr>
                                                <td>
                                                    <a href="<?=home_url()?>/estoque/" style="text-decoration: none; color: #000;">
                                                        <?=$produto[0]?>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                        <?php if($produto[1] > 0): ?>
                                                            <span class="label label-warning">
                                                                <?=$produto[1]?> dia(s) restantes de validez
                                                            </span>
                                                        <?php else: ?>
                                                            <span class="label label-danger">
                                                                Este produto vence hoje!
                                                            </span>
                                                        <?php endif; ?>
                                                </td>
                                            </tr>
                                    <?php endforeach; endif; ?>

                                    <?php
                                    $produtos_vencidos = getExpirateProducts();

                                    if(!empty($produtos_vencidos)):
                                        foreach($produtos_vencidos as $produto):
                                    ?>
                                            <tr>
                                                <td>
                                                    <a href="<?=home_url()?>/estoque/" style="text-decoration: none; color: #000;">
                                                        <?=$produto?>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <span class="label label-danger">
                                                        Produto passou do prazo de validade
                                                    </span>
                                                </td>
                                            </tr>
                                    <?php endforeach; endif; ?>

                                    <?php
                                    $produtos_para_acabar = getProductsCloseToEmpty();

                                    if(!empty($produtos_para_acabar)):
                                        foreach($produtos_para_acabar as $produto):
                                    ?>
                                            <tr>
                                                <td>
                                                    <a href="<?=home_url()?>/estoque/" style="text-decoration: none; color: #000;">
                                                        <?=$produto[0]?>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <span class="label label-warning">
                                                        <?=number_format($produto[1],2)?> unidade(s) restante(s) no estoque
                                                    </span>
                                                </td>
                                            </tr>
                                    <?php endforeach; endif; ?>

                                    <?php
                                    $produtos_acabados = getEmptyProducts();

                                    if(!empty($produtos_acabados)):
                                        foreach($produtos_acabados as $produto):
                                    ?>
                                            <tr>
                                                <td>
                                                    <a href="<?=home_url()?>/estoque/" style="text-decoration: none; color: #000;">
                                                        <?=$produto?>
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <span class="label label-danger">
                                                        Produto em falta no estoque
                                                    </span>
                                                </td>
                                            </tr>
                                    <?php endforeach; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table = $('.panel-body #table-stock tbody');
    var rows = table.find('tr');
</script>
<script type="text/javascript" src="<?=assets_url()?>/js/filter.js"></script>
<script type="text/javascript" src="<?=assets_url()?>/js/estoque-filtro.js"></script>
<?php get_footer(); ?>