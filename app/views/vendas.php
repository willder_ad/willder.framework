<?php get_header(); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <i class="fa fa-money"></i>
                    Todas <small>vendas</small>
                </h1>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelRelatorio">
                        <i class="fa fa-file-text fa-fw"></i>
                        Relatório de margem de lucro por intervalo de datas
                    </a>
                </h3>
            </div>
            <div id="collapsePainelRelatorio" class="panel-collapse collapse">
                <div class="panel-body">
                    <form action="<?=assets_url()?>/scripts/pdf-generator.php" method="post" role="form">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>
                                        Data de inicio
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="date" class="form-control date_begin" name="data-inicio" required>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Data Final
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="date" class="form-control date_end" name="data-final" data-toggle="popover" data-placement="top" data-content="Data final deve ser maior que a inicial" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <span class="text-danger">*</span>
                                    Campo obrigatório
                                </div>
                                <div class="pull-right">
                                    <div class="col-lg-6">
                                        <input type="submit" class="btn btn-primary" value="Gerar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a data-toggle="collapse" style="text-decoration: none;" data-parent="#accordion" href="#collapsePainelSearch">
                        <i class="fa fa-search fa-fw"></i>
                        Filtrar por pesquisa
                    </a>
                </h3>
            </div>
            <div id="collapsePainelSearch" class="panel-collapse collapse">
                <div class="panel-body">
                    <label>Nome do produto vendido</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="search form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <h3 class="panel-title" style="margin-top: 10px !important;">
                            <i class="fa fa-cubes fa-fw"></i> Produtos
                        </h3>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="pull-right">
                            <select id="filter" class="form-control">
                                <option value="">Filtrar</option>
                                <option value="asc_name@1">Nome (A-Z)</option>
                                <option value="desc_name@1">Nome (Z-A)</option>
                                <option value="desc_quantity@2">Maior Quantidade</option>
                                <option value="asc_quantity@2">Menor Quantidade</option>
                                <option value="date@3">Data da venda</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table-stock">
                                <thead class="text-center" style="background-color: #333; color: #fff;">
                                    <tr>
                                        <td>Nome do produto</td>
                                        <td>Quantidade vendida</td>
                                        <td>Data da venda</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $vendas = App\Model\Selling::listAllSellings();

                                    if(!empty($vendas)):
                                        foreach($vendas as $venda):
                                    ?>
                                        <tr>
                                            <td><?=$venda[0]?></td>
                                            <td width="10%" class="text-center"><?=number_format($venda[1], 2, ',','')?></td>
                                            <td width="20%" class="text-center"><?=date('d/m/Y',strtotime($venda[2]))?></td>
                                        </tr>
                                    <?php endforeach; else: ?>
                                        <tr>
                                            <td  colspan="4">
                                                Nenhuma venda realizada
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var table = $('.panel-body #table-stock tbody');
    var rows = table.find('tr');
</script>
<script type="text/javascript" src="<?=assets_url()?>/js/filter.js"></script>
<script type="text/javascript" src="<?=assets_url()?>/js/estoque-filtro.js"></script>
<?php get_footer(); ?>