<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve selecionar primeiro um produto!";
    header("Location: ".home_url()."/estoque/");
    die();
}

$_SESSION['produto-id'] = $_POST['produto-id'];

header("Location: ".home_url()."/caixa/");