<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve preencher os formulários primeiro!";
    header("Location: ".home_url().'/perfil/');
    die();
}

$type = $_POST['type'];

switch ($type) {
    case '1':
        $user_email    = $_POST['user-email'];
        $user_nickname = $_POST['user-nickname'];

        $user = new App\Model\User();
        $user->_setEmailUser($user_email);
        $user->_setUserNickname($user_nickname);

        $res = $user->changeReadConfig($user);

        if($res){
            $_SESSION['success'] = "Configurações de leitura serão validadas após LogOff";
        }else{
            $_SESSION['danger'] = "Erro ao atualizar configurações de leitura!";
        }
        header('Location: '.home_url().'/perfil/');
    break;

    case '2':
        $new_pass = $_POST['new-pass'];

        $user = new App\Model\User();
        $user->_setSenhaUser($new_pass);

        $res = $user->changePassConfig($user);

        if($res){
            $_SESSION['success'] = "Configurações de senha serão validadas após LogOff";
        }else{
            $_SESSION['danger'] = "Erro ao atualizar configurações de senha!";
        }
        header('Location: '.home_url().'/perfil/');
    break;

    case '3':
        $show_notification = $_POST['show-notification'];
        $quantity_limit    = $_POST['quantity-limit'];
        $date_limit        = $_POST['date-limit'];

        $config = new App\Model\Config();
        $config->_setShowNotifications($show_notification);
        $config->_setLimitStock($quantity_limit);
        $config->_setLimitDate($date_limit);

        $res = $config->setConfig($config);

        if($res){
            $_SESSION['success'] = "Configurações de notificações atualizadas.";
        }else{
            $_SESSION['danger'] = "Erro ao atualizar configurações de notificações!";
        }
        header('Location: '.home_url().'/perfil/');
    break;
}
