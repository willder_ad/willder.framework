<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?=assets_url()?>/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <h4 class="text-center"><?=strtoupper($_SESSION['company'].' urucutuba')?></h4>
    <p class="text-center"><strong>CUPOM NÃO FISCAL</strong></p>
    <hr>
    <?php
    $items       = App\Model\ItemSelling::getSellingToPrint();
    $total_venda = 0;
    ?>
    <table class="table table-responsive" style="font-size: 8px;">
        <thead>
            <tr>
                <td>COD</td>
                <td>DESCRIÇÃO</td>
                <td>QTD.</td>
                <td>PRECO</td>
                <td>TOTAL</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($items as $item): ?>
                <tr>
                    <td><?=$item['bar_code']?></td>
                    <td><?=$item['name_product']?></td>
                    <td><?=$item['selling_quantity']?></td>
                    <td>R$ <?=number_format($item['price'],2,',','.')?></td>
                    <td>R$ <?=number_format($item['total_item'],2,',','.')?></td>
                </tr>
            <?php $total_venda = $item['total_venda']; endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">TOTAL</td>
                <td>R$ <?=number_format($total_venda,2,',','.')?></td>
            </tr>
        </tfoot>
    </table>
    <hr>
    <p class="text-center">OBRIGADO E VOLTE SEMPRE</p>
</body>
<script>
    window.print();
    setTimeout(function() {
        window.location.href = "<?=home_url()?>/caixa/";
    }, 0);
</script>
</html>