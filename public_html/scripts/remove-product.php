<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você não selecionou um produto!";
    header("Location: ".home_url().'/estoque/');
    die();
}

$produto_id = $_POST['produto-id'];
$res        = App\Model\Product::deleteProduct($produto_id);

if($res){
    $_SESSION['success'] = "Produto excluido com sucesso!";
}else{
    $_SESSION['danger'] = "Erro ao excluir produto!";
}
header('Location: '.home_url().'/estoque/');
