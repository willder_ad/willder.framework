<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você não selecionou um fornecedor!";
    header("Location: ".home_url().'/fornecedores/');
    die();
}

$id_provider = $_POST['id-provider'];

$res = App\Model\Provider::removeProvider($id_provider);

if($res){
	$_SESSION['success'] = "Fornecedor removido com sucesso!";
}else{
	$_SESSION['danger'] = "Erro ao remover fornecedor!";
}
header("Location: ".home_url()."/fornecedores/");
