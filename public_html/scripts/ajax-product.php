<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_REQUEST)){
    $_SESSION['danger'] = "Acesso negado";
    header("Location: ".home_url()."/painel/");
    die();
}

$bar_code = $_REQUEST['code'];
$produto = App\Model\Product::listAllProductsToSell($bar_code);

echo json_encode($produto);