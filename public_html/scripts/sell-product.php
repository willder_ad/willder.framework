<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve escolher um produto para venda";
    header("Location: ".home_url()."/estoque/");
    die();
}

$id_produto       = $_POST['produto-id'];
$quantidade_venda = $_POST['quantidade-venda'];
$total_venda      = $_POST['total-venda'];

$selling = new App\Model\Selling();
$selling->_setIdProduct($id_produto);
$selling->_setSellingQuantity($quantidade_venda);
$selling->_setSellingDate(date('Y-m-d'));
$selling->_setTotalSelling($total_venda);

$res = $selling->doSelling($selling);

if($res){
    $_SESSION['success'] = "Venda realizada com sucesso!";
}else{
    $_SESSION['danger'] = "Erro ao efetuar venda!";
}
header('Location: '.home_url().'/estoque/');
