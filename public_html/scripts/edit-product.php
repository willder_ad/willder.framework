<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Acesso negado";
    header("Location: ".home_url().'/estoque/');
    die();
}

$produto_id            = $_POST['produto-id'];
$nome_produto          = $_POST['nome-produto'];
$preco_compra          = $_POST['preco-compra'];
$preco_revenda         = $_POST['preco-revenda'];
$id_fornecedor		   = $_POST['fornecedor'];
$quantidade_estoque    = $_POST['quantidade-estoque'];
$data_validade         = $_POST['data-validade'];
$tipo_unitario_produto = $_POST['tipo-unitario-produto'];
$bar_code              = $_POST['codigo-barra'];
$empresa_produto       = $_SESSION['company'];

$provider = new App\Model\Provider();
$provider->_setIdProvider($id_fornecedor);

$produto = new App\Model\Product();
$produto->_setIdProduct($produto_id);
$produto->_setNameProduct($nome_produto);
$produto->_setPurchasePriceProduct($preco_compra);
$produto->_setResalePriceProduct($preco_revenda);
$produto->_setProvider($provider);
$produto->_setStockQuantityProduct($quantidade_estoque);
$produto->_setExpirationDate($data_validade);
$produto->_setUnitTypeProduct($tipo_unitario_produto);
$produto->_setBarCode($bar_code);
$produto->_setCompany($empresa_produto);

$res = $produto->editProduct($produto);

if($res){
    $_SESSION['success'] = "Produto editado com sucesso!";
}else{
    $_SESSION['danger'] = "Erro ao editar produto!";
}
header('Location:'.home_url().'/estoque/');