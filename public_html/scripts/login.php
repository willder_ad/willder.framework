<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once VENDOR."/autoload.php";
require_once "../../bootstrap/functions.php";

session_start();

if(empty($_POST)){
    $_SESSION['danger'] = "Acesso negado!";
    header("Location: ".home_url());
    die();
}

$email   = $_POST['email'];
$senha   = $_POST['senha'];

if(isset($_POST['lembrar']))
    $token = true;
else
    $token = false;


$res = logIn($email,$senha,$token);

if($res){
    $_SESSION['success'] = "Bem vindo(a) ".$_SESSION['user'];
    header('Location: '.home_url().'/painel/');
}else{
    header('Location: '.home_url());
}
