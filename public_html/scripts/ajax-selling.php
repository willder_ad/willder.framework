<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";


session_start();
checkUser();

if(empty($_REQUEST)){
    $_SESSION['danger'] = "Acesso negado";
    header("Location: ".home_url()."/painel/");
    die();
}

$params  = $_REQUEST['selling'];
$total   = $params['total'];
$date    = date('Y-m-d');

$selling = new App\Model\Selling();
$selling->_setSellingDate($date);
$selling->_setTotalSelling($total);

$res = $selling->newSelling($selling);

if($res){
    foreach ($params['items'] as $item) {
        $selling = new App\Model\Selling();
        $selling->_setIdSelling($selling->getLastSellingId());

        $product = new App\Model\Product();
        $product->_setIdProduct($item['id']);

        $itemSelling = new App\Model\ItemSelling();
        $itemSelling->_setSelling($selling);
        $itemSelling->_setProduct($product);
        $itemSelling->_setQuantity($item['quantity']);

        $res = $itemSelling->addItem($itemSelling);
    }

    if($res){
        $_SESSION['success'] = "Venda Realizada com sucesso";

        echo json_encode(array("res"=>true));
    }else{
        $_SESSION['danger'] = "Erro ao gravar Items";
        echo json_encode(array("res"=>false));
    }
}else{
    $_SESSION['danger'] = "Erro ao gravar venda!";
    echo json_encode(array("res"=>false));
}
