<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Acesso negado";
    header("Location: ".home_url().'/estoque/');
    die();
}

$id_provider   = $_POST['id-provider'];
$name_provider = $_POST['name-provider'];

$provider = new App\Model\Provider();
$provider->_setIdProvider($id_provider);
$provider->_setNameProvider($name_provider);

$res = $provider->editProvider($provider);

if($res){
	$_SESSION['success'] = "Fornecedor editado com sucesso!";
}else{
	$_SESSION['danger'] = "Erro ao editar fornecedor!";
}
header("Location: ".home_url()."/fornecedores/");
