<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve selecionar primeiro um produto!";
    header("Location: ".home_url()."/estoque/");
    die();
}

$produto_id = $_POST['produto-id'];
$quantidade = $_POST['quantidade'];

$res = App\Model\Product::addToStock($quantidade,$produto_id);

if($res){
    $_SESSION['success'] = "Estoque atualizado com sucesso!";
}else{
    $_SESSION['danger'] = "Erro ao atualizar estoque!";
}
header("Location: ".home_url()."/estoque/");
