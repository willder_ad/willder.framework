<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/mpdf60/mpdf.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve escolher um período!";
    header("Location".home_url()."/vendas/");
    die();
}

$data_inicio = $_POST['data-inicio'];
$data_final  = $_POST['data-final'];
$assets_url  = assets_url();
$company     = $_SESSION['company'];

$html  = '<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Relatório-vendas-'.date("d-m-Y").'.pdf</title>
        <link rel="stylesheet" href="'.$assets_url.'/css/style.css" media="all" />
        <link rel="stylesheet" href="'.$assets_url.'/css/bootstrap.min.css" media="all" />
    </head>
    <body>
        <header class="clearfix">
            <div style="float:left;">
                <h2 class="name">'.ucfirst($company).'</h2>
            <div>Bom Jardim, Urucutuba</div>
        </div>
            <div>
                <div>Data de emissão: '.date('d/m/Y').'</div>
            </div>
        </header>
        <main>
            <div id="details" class="clearfix">
                <div id="client">
                    <h1>Período</h1>
                    <div class="date">Data Inicial: '.date('d/m/Y',strtotime($data_inicio)).'</div>
                    <div class="date">Data Final: '.date('d/m/Y',strtotime($data_final)).'</div>
                </div>
            </div>
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th class="no">#</th>
                        <th class="qty">NOME DO PRODUTO</th>
                        <th class="unit">PREÇO UNITÁRIO (compra)</th>
                        <th class="qty">PREÇO UNITÁRIO (revenda)</th>
                        <th class="unit">QUANTIDADE VENDIDA</th>
                        <th class="qty">DATA DA VENDA</th>
                        <th class="unit">VALOR LUCRO (R$)</th>
                        <th class="total">TOTAL DA VENDA (R$)</th>
                    </tr>
                </thead>
                <tbody>';

$vendas = App\Model\Selling::getVendasPerPeriod($data_inicio,$data_final);
$count  = 0;

if(!empty($vendas)){
    foreach($vendas as $venda){
        $html .=    '<tr>
                        <td class="no">'.++$count.'</td>
                        <td class="qty">'.$venda[0].'</td>
                        <td class="unit">R$ '.number_format($venda[1],2,',','.').'</td>
                        <td class="qty">R$ '.number_format($venda[2],2,',','.').'</td>
                        <td class="unit">'.$venda[3].'</td>
                        <td class="qty">'.date('d/m/Y',strtotime($venda[4])).'</td>
                        <td class="unit">R$ '.number_format($venda[5],2,',','.').'</td>
                        <td class="total">R$ '.number_format($venda[6],2,',','.').'</td>
                    </tr>';
    }
}else{
    $html .= '  <tr>
                    <td colspan="8">Sem vendas neste período</td>
                </tr>';
}

$html .= '          </tbody>
                    <tfoot>';

$total_de_tudo = App\Model\Selling::getTotalOfAll($data_inicio,$data_final);

if(!empty($total_de_tudo)){
    $html .= '          <tr>
                            <td colspan="7">Média do Lucro</td>
                            <td>'.number_format($total_de_tudo[0], 2, ',', '').'%</td>
                        </tr>
                        <tr>
                            <td colspan="7">Total dos lucros</td>
                            <td>R$ '.number_format($total_de_tudo[1], 2, ',', '.').'</td>
                        </tr>
                        <tr>
                            <td colspan="7">Total das vendas neste período</td>
                            <td>R$'.number_format($total_de_tudo[2], 2, ',', '.').'</td>
                        </tr>';
    }

$html .= '          </tfoot>
                </table>
            </main>
            <footer>
            '.ucfirst($company).' Urucutuba, Bom jardim © Ltda. Todos os direitos reservados.
            </footer>
        </body>
        </html>';

$mpdf = new mPDF('utf-8','A4');
$mpdf->WriteHTML($style,1);
$mpdf->WriteHTML($html);
$mpdf->Output('relatorio-vendas-'.date('d-m-Y').'.pdf','I');

exit();
