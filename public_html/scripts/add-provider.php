<?php
require_once "../../bootstrap/config.php";
require_once "../../bootstrap/helpers.php";
require_once "../../bootstrap/functions.php";
require_once VENDOR."/autoload.php";

session_start();
checkUser();

if(empty($_POST)){
    $_SESSION['danger'] = "Você deve preencher o nome do fornecedor";
    header("Location: ".home_url().'/fornecedores/');
    die();
}

$name_provider = $_POST['name-provider'];

$provider = new App\Model\Provider();
$provider->_setNameProvider($name_provider);

$res = $provider->addNewProvider($provider);

if($res){
	$_SESSION['success'] = "Fornecedor adicionado com sucesso!";
}else{
	$_SESSION['danger'] = "Erro ao adicionar fornecedor";
}
header("Location: ".home_url()."/fornecedores/");
