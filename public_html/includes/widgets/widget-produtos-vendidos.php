<?php
$produtos_mais_vendido = App\Model\Selling::getProductsMoreSelled();

if(!empty($produtos_mais_vendido)):
?>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cubes"></i>
                    Produtos mais vendidos
                </h3>
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart"></div>
                <div class="text-right">
                    <a href="<?=home_url()?>/estoque/">Ver detalhes <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        var produtos_mais_vendido = new Array();

        <?php
        foreach($produtos_mais_vendido as $produto):
        ?>
            produtos_mais_vendido.push(JSON.parse('<?=json_encode($produto)?>'));
        <?php endforeach; ?>
    </script>
<?php else: ?>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cubes"></i> Produtos mais vendidos</h3>
            </div>
            <div class="panel-body">
                <p class="alert alert-info">Hmm.. Não parece ter vendas no momento.</p>
            </div>
        </div>
    </div>
<?php endif; ?>