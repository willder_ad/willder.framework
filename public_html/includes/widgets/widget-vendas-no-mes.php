<?php
$vendas_do_dia = App\Model\Selling::getSellingPerDay();

if(!empty($vendas_do_dia)):
?>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-money"></i>
                    Número de vendas por dia
                </h3>
            </div>
            <div class="panel-body">
                <div id="morris-line-chart"></div>
                <div class="text-right">
                    <a href="<?=home_url()?>/estoque/">Ver detalhes <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        var vendas_do_dia = new Array();

        <?php
        foreach($vendas_do_dia as $venda):
        ?>
            vendas_do_dia.push(JSON.parse('<?=json_encode($venda)?>'));
        <?php endforeach; ?>
    </script>
<?php else: ?>
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Número de vendas por dia</h3>
            </div>
            <div class="panel-body">
                <p class="alert alert-info">Hmm.. Não parece ter vendas no momento.</p>
            </div>
        </div>
    </div>
<?php endif; ?>