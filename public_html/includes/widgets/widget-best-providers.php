<?php
$melhores_fornecedores = App\Model\Selling::getBestProviders();

if(!empty($melhores_fornecedores)):
?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cubes"></i>
                    Fornecedores mais em conta
                </h3>
            </div>
            <div class="panel-body">
                <div id="morris-bar-chart-providers"></div>
                <div class="text-right">
                    <a href="<?=home_url()?>/fornecedores/">Ver detalhes <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        var melhores_fornecedores = new Array();

        <?php
        foreach($melhores_fornecedores as $fornecedores):
        ?>
            melhores_fornecedores.push(JSON.parse('<?=json_encode($fornecedores)?>'));
        <?php endforeach; ?>
    </script>
<?php else: ?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cubes"></i> Fornecedores mais em conta</h3>
            </div>
            <div class="panel-body">
                <p class="alert alert-info">Hmm.. Não existe fornecedores no momento.</p>
            </div>
        </div>
    </div>
<?php endif; ?>