<?php
$lucro_por_produto = App\Model\Selling::getLucroPerProduct();

if(!empty($lucro_por_produto)):
?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Produtos que mais tiveram lucro</h3>
            </div>
            <div class="panel-body">
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-pie-chart"></div>
                </div>
                <div class="text-right">
                    <a href="<?=home_url()?>/estoque/">Ver detalhes <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        var lucro_por_produto = new Array();

        <?php
        foreach($lucro_por_produto as $produto):
        ?>
            lucro_por_produto.push(JSON.parse('<?=json_encode($produto)?>'));
        <?php endforeach; ?>
    </script>
<?php else: ?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Produtos que mais tiveram lucro</h3>
            </div>
            <div class="panel-body">
                <p class="alert alert-info">Hmm.. Não parece ter vendas no momento.</p>
            </div>
        </div>
    </div>
<?php endif; ?>