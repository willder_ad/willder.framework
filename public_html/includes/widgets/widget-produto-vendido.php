<?php
$produto_mais_vendido = App\Model\Selling::getProductMoreSelled();

if(!empty($produto_mais_vendido)):
?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cube fa-fw"></i>
                    Produto mais vendido
                </h3>
            </div>
            <div class="panel-body">
                <h3 class="text-center"><?=$produto_mais_vendido[0]?></h3>
                <div id="morris-donut-chart"></div>
                <div class="text-right">
                    <a href="<?=home_url()?>/estoque/">Ver detalhes <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        var produto_mais_vendido = new Array();

        <?php foreach($produto_mais_vendido as $item): ?>
            produto_mais_vendido.push('<?=$item?>');
        <?php endforeach; ?>
    </script>
<?php else: ?>
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cube fa-fw"></i> Produto mais vendido</h3>
            </div>
            <div class="panel-body">
                <p class="alert alert-info">Hmm.. Não parece ter vendas no momento.</p>
            </div>
        </div>
    </div>
<?php endif; ?>