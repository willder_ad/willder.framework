<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Frigorifco Urucutuba - Frigorifico no Bom Jardim</title>
    <link href="<?=assets_url()?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/sb-admin.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/morris.css" rel="stylesheet">
    <link href="<?=assets_url()?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="<?=assets_url()?>/js/jquery.js"></script>
    <?php session_start(); ?>
</head>
<body>
    <div class="text-center">
        <?php
        alert('danger');
        alert('success');
        alert('warning');
        checkUser();
        ?>
    </div>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=home_url()?>/painel/">
                    <?=ucfirst($_SESSION['company']);?>
                </a>
            </div>
            <ul class="nav navbar-right top-nav">
                <?php
                if(isset($_SESSION['show_notifications'])):
                    if($_SESSION['show_notifications'] && (($_SESSION['limit_stock']) || ($_SESSION['limit_date']))):
                        $quantity_notifications = countNotifications();

                        if($quantity_notifications):
                ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell"></i>
                                    <span class="badge" style="background-color: #d9534f;">
                                        <?=countNotifications()?>
                                    </span>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu alert-dropdown">
                                    <?php
                                    $produtos_para_vencer = getProductsCloseToExpirate();

                                    if(!empty($produtos_para_vencer)):
                                    ?>
                                        <li>
                                            <a href="<?=home_url()?>/estoque/">
                                                <?=$produtos_para_vencer[0][0]?>
                                                <span class="label label-warning">
                                                    <?php if($produtos_para_vencer[0][1] > 0):?>
                                                        <?=$produtos_para_vencer[0][1]?> dia(s) para vencer
                                                    <?php else: ?>
                                                        Este produto vence hoje!
                                                    <?php endif; ?>
                                                </span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php
                                    $produtos_vencidos = getExpirateProducts();

                                    if(!empty($produtos_vencidos)):
                                    ?>
                                        <li>
                                            <a href="<?=home_url()?>/estoque/">
                                                <?=$produtos_vencidos[0]?>
                                                <span class="label label-danger">Produto vencido</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php
                                    $produtos_para_acabar = getProductsCloseToEmpty();

                                    if(!empty($produtos_para_acabar)):
                                    ?>
                                        <li>
                                            <a href="<?=home_url()?>/estoque/">
                                                <?=$produtos_para_acabar[0][0]?>
                                                <span class="label label-warning">
                                                    <?=number_format($produtos_para_acabar[0][1],2)?> unidade(s) restantes
                                                </span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php
                                    $produtos_acabados = getEmptyProducts();

                                    if(!empty($produtos_acabados)):
                                    ?>
                                        <li>
                                            <a href="<?=home_url()?>/estoque/">
                                                <?=$produtos_acabados[0]?>
                                                <span class="label label-danger">Produto acabou</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?=home_url()?>/notificacoes/">Ver todas</a>
                                    </li>
                                </ul>
                            </li>
                <?php endif; endif; endif; ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <?=$_SESSION['nick']?$_SESSION['nick']:$_SESSION['user']?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?=home_url()?>/perfil/"><i class="fa fa-fw fa-gear"></i> Perfil</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=assets_url()?>/scripts/logoff.php">
                                <i class="fa fa-fw fa-power-off"></i> Sair
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="<?=home_url()?>/painel/"><i class="fa fa-fw fa-dashboard"></i> Painel Inicial</a>
                    </li>
                    <li>
                        <a href="<?=home_url()?>/novo/"><i class="fa fa-fw fa-plus-circle"></i> Novo Produto</a>
                    </li>
                    <li>
                        <a href="<?=home_url()?>/estoque/"><i class="fa fa-fw fa-cubes"></i> Estoque</a>
                    </li>
                    <li>
                        <a href="<?=home_url()?>/fornecedores/"><i class="fa fa-fw fa-users"></i> Fornecedores</a>
                    </li>
                    <?php if(!isMobile()): ?>
                        <li>
                            <a href="<?=home_url()?>/caixa/"><i class="fa fa-fw fa-money"></i> Caixa</a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?=home_url()?>/relatorio/"><i class="fa fa-fw fa-file-text"></i> Relatório</a>
                    </li>
                    <li>
                        <a href="<?=home_url()?>/vendas/"><i class="fa fa-fw fa-shopping-cart"></i> Vendas Realizadas</a>
                    </li>
                </ul>
            </div>
        </nav>