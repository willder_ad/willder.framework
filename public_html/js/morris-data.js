// Morris.js Charts sample data for SB Admin template
$(function() {

    // Flot Pie Chart with Tooltips
    var data = $.each(lucro_por_produto, function(key,val){return val});

    var plotObj = $.plot($("#flot-pie-chart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

    // Donut Chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Quantidade vendida",
            value: produto_mais_vendido[1]
        }, {
            label: "Total bruto da venda",
            value: produto_mais_vendido[3]
        }, {
            label: "Quantidade restante",
            value: produto_mais_vendido[2]
        }],
        resize: true
    });

    // Bar Chart
    Morris.Bar({
        element: 'morris-bar-chart',
        data: $.each(produtos_mais_vendido, function(key,val){return val}),
        xkey: 'product',
        ykeys: ['quantity'],
        labels: ['Quantidade'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });

    // Bar Chart
    Morris.Bar({
        element: 'morris-bar-chart-providers',
        data: $.each(melhores_fornecedores, function(key,val){return val}),
        xkey: 'provider',
        ykeys: ['purchase'],
        labels: ['Média dos produtos (R$)'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });

    // Line Chart
    Morris.Line({
        element: 'morris-line-chart',
        data: $.each(vendas_do_dia, function(key,val){return val}),
        xkey: 'day',
        ykeys: ['quantity'],
        labels: ['Quantidade'],
        smooth: false,
        resize: true
    });
});
