var column_content  = [];

var prepareFilter = {
    fillContent: function(col) {
        var columns = rows.children('td:nth-child('+col+')');

        $.each(columns,function(key, val){
            column_content.push(val.textContent+"@"+key);
        });
    },
    sortContent: function(isNumber) {
        if(isNumber){
            column_content.sort(function(val1, val2){
                val1 = val1.split('@');
                val2 = val2.split('@');

                return parseFloat(val1[0]) - parseFloat(val2[0]);
            });
        }else{
            column_content.sort(function(val1, val2){
                val1 = val1.split('@');
                val2 = val2.split('@');

                return val1[0].toLowerCase().localeCompare(val2[0].toLowerCase());
            });
        }
    },
    reverseContent: function(isNumber){
        if(isNumber){
            column_content.sort(function(val1, val2){
                val1 = val1.split('@');
                val2 = val2.split('@');

                return (parseFloat(val1[0]) - parseFloat(val2[0])) * (-1);
            });
        }else{
            column_content.sort(function(val1, val2){
                val1 = val1.split('@');
                val2 = val2.split('@');

                return (val1[0].toLowerCase().localeCompare(val2[0].toLowerCase())) * (-1);
            });
        }
    },
    orderRows: function() {
        rows.remove();

        $.each(column_content,function(key,val){
            var key   = val.split('@');

            table.append(rows[key[1]]);
        });
    }
};

var filter = {
    sortAsc: function(col,isNumber){
        column_content = [];
        prepareFilter.fillContent(col);
        prepareFilter.sortContent(isNumber);
        prepareFilter.orderRows();
    },
    sortDesc: function(col,isNumber){
        column_content = [];
        prepareFilter.fillContent(col);
        prepareFilter.reverseContent(isNumber);
        prepareFilter.orderRows();
    }
}