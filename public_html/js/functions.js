function choice(){
    var choice = confirm('Deseja remover este item?');

    return choice;
}


jQuery(function($){
    var new_pass         = $('.form-group .new_pass');
    var confirm_new_pass = $('.form-group .confirm_new_pass');
    var quantity_limit   = $('.form-group .quantity_limit');
    var date_limit       = $('.form-group .date_limit');
    var sell_quantity    = $('.form-group .sell_quantity');
    var quantity_stock   = $('.form-group .quantity_stock');
    var price_per_unit   = $('.form-group .price_per_unit');
    var total_selling    = $('.form-group .total_selling');
    var date_begin       = $('.form-group .date_begin');
    var date_end         = $('.form-group .date_end');

    confirm_new_pass.on('focusout',function(e){
        e.preventDefault();

        if(confirm_new_pass.val() !== new_pass.val()){
            confirm_new_pass.css({"border":"1px solid red"});
            confirm_new_pass.val('');
            $('[data-toggle="popover"]').popover('show');
        }else{
            confirm_new_pass.css({"border":"1px solid green"});
            $('[data-toggle="popover"]').popover('destroy');
        }
    });

    quantity_limit.on('focusout',function(){
        if(quantity_limit.val())
            date_limit.removeAttr('required');
        else
            date_limit.attr('required','required');
    });

    date_limit.on('focusout',function(){
        if(date_limit.val())
            quantity_limit.removeAttr('required');
        else
            quantity_limit.attr('required','required');
    });

    date_end.on('focusout', function(e){
        e.preventDefault();

        if(date_end.val() < date_begin.val()){
            date_end.css({"border":"1px solid red"});
            date_end.val('');
            $('[data-toggle="popover"]').popover('show');
        }else{
            date_end.css({"border":"1px solid green"});
            $('[data-toggle="popover"]').popover('destroy');
        }
    });
});