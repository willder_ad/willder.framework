var AjaxRequest = {
    getProduct: function(bar_code, callback) {
        var url = assets_url+"/scripts/ajax-product.php";

        $.getJSON(url,{code: bar_code}, function(data){
            callback(data);
        });
    },
    doSelling: function(data_selling, callback) {
        var url = assets_url+"/scripts/ajax-selling.php";

        $.getJSON(url,data_selling, function(data) {
            callback(data);
        });
    }
};