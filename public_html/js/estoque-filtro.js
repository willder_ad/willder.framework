jQuery(function($){
    $(window).on('load',function(){

        var filter_selector = $('.panel-heading #filter');
        var search_selector = $('input.search');

        filter_selector.on('change',function(){
            var filter_selected = filter_selector.find('option:selected').val().split('@');

            switch(filter_selected[0]){
                case 'asc_name':
                    filter.sortAsc(filter_selected[1],false);
                break;
                case 'desc_name':
                    filter.sortDesc(filter_selected[1],false);
                break;
                case 'desc_price':
                    filter.sortDesc(filter_selected[1],true);
                break;
                case 'asc_price':
                    filter.sortAsc(filter_selected[1],true);
                break;
                case 'desc_quantity':
                    filter.sortDesc(filter_selected[1],true);
                break;
                case 'asc_quantity':
                    filter.sortAsc(filter_selected[1],true);
                break;
                case 'date':
                    filter.sortDesc(filter_selected[1],false);
                break;
            }
        });

        search_selector.on('keyup', function() {
            var searched = $(this).val().toUpperCase();
            var column   = rows.children("td:nth-child(1)");

            rows.show();

            column.each(function(){
                if($(this).text().toUpperCase().indexOf(searched) < 0){
                    $(this).parent().hide();
                }
            });
        });

        search_selector.on('focusout', function() {
            $(this).val("");
        });
    });
});