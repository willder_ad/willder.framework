var id_product	   = $('.id-product');
var product_price  = $('.product-price');
var bar_code       = $('.bar-code');
var name_product   = $('.name-product');
var stock_quantity = $('.stock-quantity');
var sell_quantity  = $('.sell-quantity');
var table_cart     = $('.table tbody');
var default_row    = $('tr[class="default"]');
var total_selling  = $('.total');
var end_selling    = $('.end-selling');

var cart = {
	resetValues: function() {
		bar_code.val('');
		name_product.val('');
		stock_quantity.val('');
		sell_quantity.val('1');
		bar_code.focus();
	},
	newRow: function() {
		let row = {
			openRow:"<tr>",
			closeRow:"</tr>"
		};

		return row;
	},
	newColumn: function() {
		let column = {
			openColumn:"<td class='text-center'>",
			closeColumn:"</td>"
		};

		return column;
	},
	newItem: function(name_product, quantity, price){
		let row    = cart.newRow();
		let column = cart.newColumn();

		element  = row.openRow;
		element += column.openColumn;
		element += name_product;
		element += column.closeColumn;
		element += column.openColumn;
		element += quantity;
		element += column.closeColumn;
		element += column.openColumn;
		element += price;
		element += column.closeColumn;
		element += column.openColumn;
		element += '<button class="btn btn-danger" title="Remover do carrinho" onclick="cart.removeItem(this)">'+
				   '<i class="fa fa-remove"></i>'+
				   '</button>';
		element += column.closeColumn;
		element += row.closeRow;

		return element;
	},
	appendElement: function() {
		let name 	 = name_product.val();
		let quantity = sell_quantity.val();
		let price 	 = product_price.val();

		if(default_row.length > 0){
			default_row.remove();
		}

		table_cart.append(cart.newItem(name, quantity, price));
		table_cart.find('tr').last().attr('data-id',id_product.val());
		bar_code.focus();
		total_selling.text(parseFloat(total_selling.text()) + parseFloat(price * quantity));
	},
	removeItem: function(element){
		let choice     = confirm("Deseja remover do carrinho?");

		if(choice){
			let row      = $(element).closest('tr');
			let price    = parseFloat(row.find('td:nth-child(3)').text());
			let quantity = parseFloat(row.find('td:nth-child(2)').text());

			total_selling.text(parseFloat(total_selling.text()) - parseFloat(price * quantity));
			row.remove();
			cart.resetValues();
			bar_code.focus();

			let count_rows = table_cart.find('tr').length;

			if(count_rows == 0)
				table_cart.append(default_row);
		}
	}
};

$(window).on('load', function() {
	bar_code.focus();
});

$('input').keydown(function(e) {
	var code = null;
    code = (e.keyCode ? e.keyCode : e.which);

    if(code == 27){
    	cart.resetValues();
    	bar_code.focus();
    }
});

$('input').keypress(function (e) {
    var code = null;
    code = (e.keyCode ? e.keyCode : e.which);

    if(code == 13){
    	if($(this).hasClass('bar-code'))
    		sell_quantity.focus();
    	else{
    		if(sell_quantity.val() > stock_quantity.val() || (sell_quantity.val() == "")){
    			$('[data-toggle="popover"]').popover('show');
    			sell_quantity.focus();
    			sell_quantity.select();
    			sell_quantity.css({'border':'1px solid red'});
    		}else{
    			$('[data-toggle="popover"]').popover('destroy');
    			sell_quantity.css({'border':'1px solid #ccc'});
    			table_cart.append(cart.appendElement());
    			cart.resetValues();
    		}
    	}
    }
});

$(window).on('beforeunload', function(){
	if($('.table tbody tr').not('[class="default"]').length > 0)
		return 'Se você sair agora perderá os dados da venda.';
});

bar_code.on('focusout', function() {

	if(!bar_code.val())
		return null;

	AjaxRequest.getProduct(bar_code.val(),function(data){

		if(data.name_product == undefined){
			bar_code.attr('data-content','Produto não encontrado ou fora de estoque!');
			$('[data-toggle="bar-code"]').popover('show');
			bar_code.css({'border':'1px solid red'});
			bar_code.focus();
			bar_code.select();
		}else{
			if(table_cart.find('[data-id="'+data.id_product+'"]').length > 0){
				bar_code.attr('data-content','Produto já esta no carrinho!');
				$('[data-toggle="bar-code"]').popover('show');
				bar_code.css({'border':'1px solid red'});
				bar_code.focus();
				bar_code.select();
			}else{
				$('[data-toggle="bar-code"]').popover('destroy');
				bar_code.css({'border':'1px solid #ccc'});
			}
		}

		let quantity = data.stock_quantity_product;

		if(data.unit_type_product == "kg")
			quantity = parseFloat(data.stock_quantity_product).toFixed(2);

		id_product.val(data.id_product);
		product_price.val(data.resale_price_product);
		name_product.val(data.name_product);
		stock_quantity.val(quantity);
	});

	sell_quantity.focus();
	sell_quantity.select();
});

end_selling.on('click', function() {
	let rows = table_cart.find('tr').not('[class="default"]');

	if(rows.length == 0)
		$('[data-toggle="end-sell"]').popover('show');
	else{
		let choice = confirm('Deseja finalizar a venda?');

		if(choice){
			$('[data-toggle="end-sell"]').popover('destroy');

			let data  = [];
			let total = parseFloat($('.total').text());

			$.each(rows, function(key, val) {
				let product_id    = $(this).data('id');
				let quantity 	  = 0;
				let colunms       = $(this).find('td').not('td:last-child');

				$.each(colunms, function(key, val) {
					if(key == 1)
						quantity = $(this).text();
				});

				data.push({id:product_id,quantity:quantity});
			});

			AjaxRequest.doSelling({selling:{items:data,total:total}}, function(result) {
				table_cart.find('tr').remove().append(default_row);

				if(result.res === true)
					window.location.href= assets_url + "/scripts/print-template.php";
			});
		}
	}
});

end_selling.on('mouseout', function() {
	$('[data-toggle="end-sell"]').popover('destroy');
	bar_code.focus();
});